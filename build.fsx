#r "paket:
nuget Fake.Core.Target
nuget Fake.IO.Zip
nuget Fake.IO.FileSystem
nuget Fake.Core.CommandLineParsing
nuget Fake.DotNet.Cli
nuget Fake.DotNet.Paket
nuget Fake.Javascript.Yarn //"
#load "./.fake/build.fsx/intellisense.fsx"

open Fake.Core
open Fake.Core
open Fake.JavaScript
open Fake.IO.Globbing.Operators
open Fake.IO
open Fake.DotNet
open System
open System.IO

let versionFromGlobalJson : DotNet.CliInstallOptions -> DotNet.CliInstallOptions = (fun o ->
        { o with Version = DotNet.Version (DotNet.getSDKVersionFromGlobalJson()) }
    )
let dotnetSdk = lazy DotNet.install versionFromGlobalJson
let inline dotnetSimple arg = DotNet.Options.lift dotnetSdk.Value arg
let solutionName = "MyFunction"
let srcFiles =
    !! (sprintf "%s.sln add src/%s.fsproj" solutionName solutionName)
    ++ (sprintf "%s.sln add tools/DotnetCLI.fsproj" solutionName )


Target.create "Clean" (fun _ ->
    !! "src/**/bin"
    ++ "src/**/obj"
    |> Shell.cleanDirs
)

Target.create "PaketInstall" (fun _ ->
  Paket.restore id
)

Target.create "DotnetRestore" (fun _ ->
    srcFiles
    |> Seq.iter (fun proj ->
        DotNet.restore id proj
))

Target.create "Build" (fun _ ->   
  Yarn.exec "run npx webpack --config ./tools/webpack.config.js" id 
)

Target.create "Watch" (fun _ ->   
  Yarn.exec """watch build ./src""" id 
)


open Fake.Core.TargetOperators

"Clean"
  ==> "PaketInstall"

"Build"

// start build
Target.runOrDefaultWithArguments "Build"