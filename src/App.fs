module App 

open Fable.Core
open Fable.Import.Unreal
open Fable.Core.JsInterop

let main() = 
  let vector = Vector.MakeVector( 100.,0.,100.)
  let rotation = Rotator.Create()
  rotation.Yaw <- 180.

  let actor : TextRenderActor = TextRenderActor.Create(GWorld, vector, rotation)
  actor.TextRender.SetHorizontalAlignment EHorizTextAligment.EHTA_Center
  actor.TextRender.SetText "Fable Fable!"

  fun () -> actor.DestroyUObject()
  
