module rec Fable.Import.Unreal

open Fable.Core
open Fable.Core.JsInterop

type [<AllowNullLiteral>] Field =
    inherit UObject
    abstract GetMetaData: Key: string -> string

type [<AllowNullLiteral>] Enum =
    inherit Field
    abstract GetEnumeratorName: EnumeratorValue: float -> string
    abstract GetEnumeratorUserFriendlyName: EnumeratorValue: float -> string
    abstract GetEnumeratorValueFromIndex: EnumeratorIndex: float -> float
    abstract GetValidValue: EnumeratorValue: float -> float

  type [<AllowNullLiteral>] Package =
    inherit UObject
    abstract DeletePackage: unit -> bool
    abstract FindWorldInPackage: unit -> World
    abstract SavePackage: Filename: string -> bool
    abstract GetLongPackagePath: unit -> string
    abstract HasAnyPackageFlags: Flags: float -> bool
    abstract LoadPackage: PackageName: string -> Package

type UnrealEngineClass =
    obj option

type timeout_handle =
    obj option

type [<AllowNullLiteral>] DirectoryItem =
    abstract Name: string with get, set
    abstract bIsDirectory: bool with get, set
    abstract clone: unit -> DirectoryItem

type [<AllowNullLiteral>] UObject =
    abstract StaticClass: obj option with get, set
    abstract ExecuteUbergraph: EntryPoint: float -> unit
    abstract AddWhitelistedObject: unit -> unit
    abstract BroadcastAssetCreated: unit -> unit
    abstract MarkPackageDirty: unit -> bool
    abstract ModifyObject: bAlwaysMarkDirty: bool -> unit
    abstract OpenEditorForAsset: unit -> bool
    abstract PostEditChange: unit -> unit
    abstract DestroyUObject: unit -> unit
    abstract CreateEnum: Name: string * DisplayNames: ResizeArray<string> -> Enum
    abstract CreatePackage: PackageName: string -> Package
    abstract Duplicate: Outer: UObject * Name: string -> UObject
    abstract FindObjectWithOuter: ClassToLookFor: UnrealEngineClass * NameToLookFor: string -> UObject
    abstract FindPackage: PackageName: string -> Package
    abstract GetArchetypePathName: unit -> string
    abstract GetDir: WhichDir: string -> string
    abstract GetFields: bIncludeSuper: bool -> ResizeArray<Field>
    abstract GetFileSize: Filename: string -> float
    abstract GetName: unit -> string
    abstract GetObjectsWithOuter: ?Results: ResizeArray<UObject> * ?bIncludeNestedObjects: bool * ?ExclusionFlags: float * ?ExclusionInternalFlags: float -> obj
    abstract GetOuter: unit -> UObject
    abstract GetOutermost: unit -> UObject
    abstract HasAnyFlags: Flags: float -> bool
    abstract ReadDirectory: Directory: string * ?OutItems: ResizeArray<DirectoryItem> -> obj
    abstract ReadFile: Filename: string -> bool
    abstract ReadStringFromFile: Filename: string -> string
    abstract SetObjectFlags: Flags: float -> unit
    abstract WriteFile: Filename: string -> bool
    abstract WriteStringToFile: Filename: string * Data: string * EncodingOptions: obj -> bool
    abstract GetDatasmithUserData: unit -> obj //DatasmithAssetUserData
    abstract GetDatasmithUserDataKeysAndValuesForValue: StringToMatch: string * ?OutKeys: ResizeArray<string> * ?OutValues: ResizeArray<string> -> obj
    abstract GetDatasmithUserDataValueForKey: Key: string -> string
    abstract RedirectVislog: DestinationOwner: UObject -> unit
    abstract Conv_ObjectToText: unit -> string
    abstract Conv_ObjectToSoftObjectReference: unit -> UObject
    abstract Conv_SoftObjectReferenceToObject: unit -> UObject
    abstract Conv_SoftObjectReferenceToString: unit -> string
    abstract CreateCopyForUndoBuffer: unit -> unit
    abstract DoesImplementInterface: Interface: UnrealEngineClass -> bool
    abstract EqualEqual_SoftObjectReference: B: UObject -> bool
    abstract GetDisplayName: unit -> string
    abstract GetObjectName: unit -> string
    abstract GetPathName: unit -> string
    abstract GetPrimaryAssetIdFromObject: unit -> obj //PrimaryAssetId
    abstract GetPrimaryAssetIdFromSoftObjectReference: unit -> obj //PrimaryAssetId
    abstract IsValid: unit -> bool
    abstract IsValidSoftObjectReference: unit -> bool
    abstract K2_ClearTimer: FunctionName: string -> unit
    abstract K2_GetTimerElapsedTime: FunctionName: string -> float
    abstract K2_GetTimerRemainingTime: FunctionName: string -> float
    abstract K2_IsTimerActive: FunctionName: string -> bool
    abstract K2_IsTimerPaused: FunctionName: string -> bool
    abstract K2_PauseTimer: FunctionName: string -> unit
    abstract K2_SetTimer: FunctionName: string * Time: float * bLooping: bool -> obj //TimerHandle
    abstract K2_TimerExists: FunctionName: string -> bool
    abstract K2_UnPauseTimer: FunctionName: string -> unit
    abstract NotEqual_SoftObjectReference: B: UObject -> bool
    abstract SetBoolPropertyByName: PropertyName: string * Value: bool -> unit
    abstract SetBytePropertyByName: PropertyName: string * Value: float -> unit
    abstract SetClassPropertyByName: PropertyName: string * Value: UnrealEngineClass -> unit
    abstract SetCollisionProfileNameProperty: PropertyName: string * Value: obj //CollisionProfileName -> unit
    abstract SetFloatPropertyByName: PropertyName: string * Value: float -> unit
    abstract SetInterfacePropertyByName: PropertyName: string * Value: obj option -> unit
    abstract SetIntPropertyByName: PropertyName: string * Value: float -> unit
    abstract SetLinearColorPropertyByName: PropertyName: string * Value: obj //LinearColor -> unit
    abstract SetNamePropertyByName: PropertyName: string * Value: string -> unit
    abstract SetObjectPropertyByName: PropertyName: string * Value: UObject -> unit
    abstract SetRotatorPropertyByName: PropertyName: string * Value: obj //Rotator -> unit
    abstract SetSoftClassPropertyByName: PropertyName: string * Value: obj //Class -> unit
    abstract SetSoftObjectPropertyByName: PropertyName: string * Value: UObject -> unit
    abstract SetStringPropertyByName: PropertyName: string * Value: string -> unit
    abstract SetStructurePropertyByName: PropertyName: string * Value: obj// GenericStruct -> unit
    abstract SetTextPropertyByName: PropertyName: string * Value: string -> unit
    abstract SetTransformPropertyByName: PropertyName: string * Value: obj//Transform -> unit
    abstract SetVectorPropertyByName: PropertyName: string * Value: Vector -> unit
    abstract TransactObject: unit -> unit
    abstract Conv_ObjectToString: unit -> string
    abstract EqualEqual_ObjectObject: B: UObject -> bool
    abstract NotEqual_ObjectObject: B: UObject -> bool
    abstract SelectObject: B: UObject * bSelectA: bool -> UObject
    abstract SetArrayPropertyByName: PropertyName: string * Value: ResizeArray<float> -> unit
    abstract GetObjectClass: unit -> UnrealEngineClass
    abstract SetSetPropertyByName: PropertyName: string * Value: obj option -> unit
    abstract SetMapPropertyByName: PropertyName: string * Value: obj option -> unit

type [<AllowNullLiteral>] UObjectStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> UObject
    [<Emit "new $0($1...)">] abstract Create: Outer: UObject -> UObject
    abstract Load: ResourceName: string -> UObject
    abstract Find: Outer: UObject * ResourceName: string -> UObject
    abstract GetClassObject: unit -> obj//Class
    abstract GetDefaultObject: unit -> UObject
    abstract GetDefaultSubobjectByName: Name: string -> UObject
    abstract SetDefaultSubobjectClass: Name: string -> unit
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> UObject
    abstract C: Other: U2<UObject, obj option> -> UObject
    abstract AddWhitelistedObject: InObject: UObject -> unit
    abstract BroadcastAssetCreated: NewAsset: UObject -> unit
    abstract MarkPackageDirty: InObject: UObject -> bool
    abstract ModifyObject: UObject: UObject * bAlwaysMarkDirty: bool -> unit
    abstract OpenEditorForAsset: Asset: UObject -> bool
    abstract PostEditChange: InObject: UObject -> unit
    abstract DestroyUObject: UObject: UObject -> unit
    abstract CreateEnum: Outer: UObject * Name: string * DisplayNames: ResizeArray<string> -> Enum
    abstract CreatePackage: Outer: UObject * PackageName: string -> Package
    abstract Duplicate: UObject: UObject * Outer: UObject * Name: string -> UObject
    abstract FindObjectWithOuter: Outer: UObject * ClassToLookFor: UnrealEngineClass * NameToLookFor: string -> UObject
    abstract FindPackage: InOuter: UObject * PackageName: string -> Package
    abstract GetArchetypePathName: UObject: UObject -> string
    abstract GetDir: UObject: UObject * WhichDir: string -> string
    abstract GetFields: UObject: UObject * bIncludeSuper: bool -> ResizeArray<Field>
    abstract GetFileSize: UObject: UObject * Filename: string -> float
    abstract GetName: UObject: UObject -> string
    abstract GetObjectsWithOuter: Outer: UObject * ?Results: ResizeArray<UObject> * ?bIncludeNestedObjects: bool * ?ExclusionFlags: float * ?ExclusionInternalFlags: float -> obj
    abstract GetOuter: UObject: UObject -> UObject
    abstract GetOutermost: UObject: UObject -> UObject
    abstract HasAnyFlags: UObject: UObject * Flags: float -> bool
    abstract ReadDirectory: UObject: UObject * Directory: string * ?OutItems: ResizeArray<DirectoryItem> -> obj
    abstract ReadFile: UObject: UObject * Filename: string -> bool
    abstract ReadStringFromFile: UObject: UObject * Filename: string -> string
    abstract SetObjectFlags: Obj: UObject * Flags: float -> unit
    abstract WriteFile: UObject: UObject * Filename: string -> bool
    abstract WriteStringToFile: UObject: UObject * Filename: string * Data: string * EncodingOptions: obj//EJavascriptEncodingOptions -> bool
    abstract GetDatasmithUserData: UObject: UObject -> obj//DatasmithAssetUserData
    abstract GetDatasmithUserDataKeysAndValuesForValue: UObject: UObject * StringToMatch: string * ?OutKeys: ResizeArray<string> * ?OutValues: ResizeArray<string> -> obj
    abstract GetDatasmithUserDataValueForKey: UObject: UObject * Key: string -> string
    abstract RedirectVislog: SourceOwner: UObject * DestinationOwner: UObject -> unit
    abstract Conv_ObjectToText: InObj: UObject -> string
    abstract Conv_ObjectToSoftObjectReference: UObject: UObject -> UObject
    abstract Conv_SoftObjectReferenceToObject: softobject: UObject -> UObject
    abstract Conv_SoftObjectReferenceToString: SoftObjectReference: UObject -> string
    abstract CreateCopyForUndoBuffer: ObjectToModify: UObject -> unit
    abstract DoesImplementInterface: TestObject: UObject * Interface: UnrealEngineClass -> bool
    abstract EqualEqual_SoftObjectReference: A: UObject * B: UObject -> bool
    abstract GetDisplayName: UObject: UObject -> string
    abstract GetObjectName: UObject: UObject -> string
    abstract GetPathName: UObject: UObject -> string
    abstract GetPrimaryAssetIdFromObject: UObject: UObject -> obj//PrimaryAssetId
    abstract GetPrimaryAssetIdFromSoftObjectReference: SoftObjectReference: UObject -> obj//PrimaryAssetId
    abstract IsValid: UObject: UObject -> bool
    abstract IsValidSoftObjectReference: SoftObjectReference: UObject -> bool
    abstract K2_ClearTimer: UObject: UObject * FunctionName: string -> unit
    abstract K2_GetTimerElapsedTime: UObject: UObject * FunctionName: string -> float
    abstract K2_GetTimerRemainingTime: UObject: UObject * FunctionName: string -> float
    abstract K2_IsTimerActive: UObject: UObject * FunctionName: string -> bool
    abstract K2_IsTimerPaused: UObject: UObject * FunctionName: string -> bool
    abstract K2_PauseTimer: UObject: UObject * FunctionName: string -> unit
    abstract K2_SetTimer: UObject: UObject * FunctionName: string * Time: float * bLooping: bool -> obj//TimerHandle
    abstract K2_TimerExists: UObject: UObject * FunctionName: string -> bool
    abstract K2_UnPauseTimer: UObject: UObject * FunctionName: string -> unit
    abstract NotEqual_SoftObjectReference: A: UObject * B: UObject -> bool
    abstract SetBoolPropertyByName: UObject: UObject * PropertyName: string * Value: bool -> unit
    abstract SetBytePropertyByName: UObject: UObject * PropertyName: string * Value: float -> unit
    abstract SetClassPropertyByName: UObject: UObject * PropertyName: string * Value: UnrealEngineClass -> unit
    abstract SetCollisionProfileNameProperty: UObject: UObject * PropertyName: string * Value: obj//CollisionProfileName -> unit
    abstract SetFloatPropertyByName: UObject: UObject * PropertyName: string * Value: float -> unit
    abstract SetInterfacePropertyByName: UObject: UObject * PropertyName: string * Value: obj option -> unit
    abstract SetIntPropertyByName: UObject: UObject * PropertyName: string * Value: float -> unit
    abstract SetLinearColorPropertyByName: UObject: UObject * PropertyName: string * Value: obj//LinearColor -> unit
    abstract SetNamePropertyByName: UObject: UObject * PropertyName: string * Value: string -> unit
    abstract SetObjectPropertyByName: UObject: UObject * PropertyName: string * Value: UObject -> unit
    abstract SetRotatorPropertyByName: UObject: UObject * PropertyName: string * Value: obj//Rotator -> unit
    abstract SetSoftClassPropertyByName: UObject: UObject * PropertyName: string * Value: obj//Class -> unit
    abstract SetSoftObjectPropertyByName: UObject: UObject * PropertyName: string * Value: UObject -> unit
    abstract SetStringPropertyByName: UObject: UObject * PropertyName: string * Value: string -> unit
    abstract SetStructurePropertyByName: UObject: UObject * PropertyName: string * Value: obj//GenericStruct -> unit
    abstract SetTextPropertyByName: UObject: UObject * PropertyName: string * Value: string -> unit
    abstract SetTransformPropertyByName: UObject: UObject * PropertyName: string * Value: obj//Transform -> unit
    abstract SetVectorPropertyByName: UObject: UObject * PropertyName: string * Value: Vector -> unit
    abstract TransactObject: UObject: UObject -> unit
    abstract Conv_ObjectToString: InObj: UObject -> string
    abstract EqualEqual_ObjectObject: A: UObject * B: UObject -> bool
    abstract NotEqual_ObjectObject: A: UObject * B: UObject -> bool
    abstract SelectObject: A: UObject * B: UObject * bSelectA: bool -> UObject
    abstract SetArrayPropertyByName: UObject: UObject * PropertyName: string * Value: ResizeArray<float> -> unit
    abstract GetObjectClass: UObject: UObject -> UnrealEngineClass
    abstract SetSetPropertyByName: UObject: UObject * PropertyName: string * Value: obj option -> unit
    abstract SetMapPropertyByName: UObject: UObject * PropertyName: string * Value: obj option -> unit
type [<StringEnum>] [<RequireQualifiedAccess>] EOrientPositionSelector =
    | [<CompiledName "Orientation">] Orientation
    | [<CompiledName "Position">] Position
    | [<CompiledName "OrientationAndPosition">] OrientationAndPosition
    | [<CompiledName "EOrientPositionSelector_MAX">] EOrientPositionSelector_MAX

type [<AllowNullLiteral>] Quat =
    abstract X: float with get, set
    abstract Y: float with get, set
    abstract Z: float with get, set
    abstract W: float with get, set
    abstract clone: unit -> Quat
    abstract SetBaseOrientation: unit -> unit

type [<AllowNullLiteral>] QuatStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Quat
    abstract C: Other: U2<UObject, obj option> -> Quat
    abstract SetBaseOrientation: InBaseOrientation: Quat -> unit

type [<StringEnum>] [<RequireQualifiedAccess>] EEasingFunc =
    | [<CompiledName "Linear">] Linear
    | [<CompiledName "Step">] Step
    | [<CompiledName "SinusoidalIn">] SinusoidalIn
    | [<CompiledName "SinusoidalOut">] SinusoidalOut
    | [<CompiledName "SinusoidalInOut">] SinusoidalInOut
    | [<CompiledName "EaseIn">] EaseIn
    | [<CompiledName "EaseOut">] EaseOut
    | [<CompiledName "EaseInOut">] EaseInOut
    | [<CompiledName "ExpoIn">] ExpoIn
    | [<CompiledName "ExpoOut">] ExpoOut
    | [<CompiledName "ExpoInOut">] ExpoInOut
    | [<CompiledName "CircularIn">] CircularIn
    | [<CompiledName "CircularOut">] CircularOut
    | [<CompiledName "CircularInOut">] CircularInOut
    | [<CompiledName "EEasingFunc_MAX">] EEasingFunc_MAX

type [<StringEnum>] [<RequireQualifiedAccess>] ELerpInterpolationMode =
    | [<CompiledName "QuatInterp">] QuatInterp
    | [<CompiledName "EulerInterp">] EulerInterp
    | [<CompiledName "DualQuatInterp">] DualQuatInterp
    | [<CompiledName "ELerpInterpolationMode_MAX">] ELerpInterpolationMode_MAX

type [<AllowNullLiteral>] Rotator =
    abstract Pitch: float with get, set
    abstract Yaw: float with get, set
    abstract Roll: float with get, set
    abstract clone: unit -> Rotator
    abstract GetBaseRotationAndBaseOffsetInMeters: ?OutBaseOffsetInMeters: Vector -> obj
    abstract GetBaseRotationAndPositionOffset: ?OutPosOffset: Vector -> obj
    abstract GetPose: ?DevicePosition: Vector * ?NeckPosition: Vector * ?bUseOrienationForPlayerCamera: bool * ?bUsePositionForPlayerCamera: bool * ?PositionScale: Vector -> obj
    abstract SetBaseRotationAndBaseOffsetInMeters: BaseOffsetInMeters: Vector * Options: EOrientPositionSelector -> unit
    abstract SetBaseRotationAndPositionOffset: PosOffset: Vector * Options: EOrientPositionSelector -> unit
    abstract SetBaseRotation: unit -> unit
    abstract Conv_RotatorToText: unit -> string
    abstract Conv_RotatorToString: unit -> string
    abstract BreakRotator: ?Roll: float * ?Pitch: float * ?Yaw: float -> obj
    abstract BreakRotIntoAxes: ?X: Vector * ?Y: Vector * ?Z: Vector -> obj
    abstract ComposeRotators: B: Rotator -> Rotator
    abstract Conv_RotatorToTransform: unit -> Transform
    abstract Conv_RotatorToVector: unit -> Vector
    abstract EqualEqual_RotatorRotator: B: Rotator * ErrorTolerance: float -> bool
    abstract GetAxes: ?X: Vector * ?Y: Vector * ?Z: Vector -> obj
    abstract GetForwardVector: unit -> Vector
    abstract GetRightVector: unit -> Vector
    abstract GetUpVector: unit -> Vector
    abstract Multiply_RotatorFloat: B: float -> Rotator
    abstract Multiply_RotatorInt: B: float -> Rotator
    abstract NegateRotator: unit -> Rotator
    abstract NormalizedDeltaRotator: B: Rotator -> Rotator
    abstract NotEqual_RotatorRotator: B: Rotator * ErrorTolerance: float -> bool
    abstract REase: B: Rotator * Alpha: float * bShortestPath: bool * EasingFunc: EEasingFunc * BlendExp: float * Steps: float -> Rotator
    abstract RInterpTo: Target: Rotator * DeltaTime: float * InterpSpeed: float -> Rotator
    abstract RInterpTo_Constant: Target: Rotator * DeltaTime: float * InterpSpeed: float -> Rotator
    abstract RLerp: B: Rotator * Alpha: float * bShortestPath: bool -> Rotator
    abstract SelectRotator: B: Rotator * bPickA: bool -> Rotator
    abstract IsValidAIRotation: unit -> bool
    abstract GetOrientationAndPosition: ?DevicePosition: Vector -> obj

type [<AllowNullLiteral>] Transform =
    abstract Rotation: Quat with get, set
    abstract Translation: Vector with get, set
    abstract Scale3D: Vector with get, set
    abstract clone: unit -> Transform
    abstract Conv_TransformToText: unit -> string
    abstract Conv_TransformToString: unit -> string
    abstract BreakTransform: ?Location: Vector * ?Rotation: Rotator * ?Scale: Vector -> obj
    abstract ComposeTransforms: B: Transform -> Transform
    abstract ConvertTransformToRelative: ParentTransform: Transform -> Transform
    abstract EqualEqual_TransformTransform: B: Transform -> bool
    abstract InverseTransformDirection: Direction: Vector -> Vector
    abstract InverseTransformLocation: Location: Vector -> Vector
    abstract InverseTransformRotation: Rotation: Rotator -> Rotator
    abstract InvertTransform: unit -> Transform
    abstract NearlyEqual_TransformTransform: B: Transform * LocationTolerance: float * RotationTolerance: float * Scale3DTolerance: float -> bool
    abstract SelectTransform: B: Transform * bPickA: bool -> Transform
    abstract TEase: B: Transform * Alpha: float * EasingFunc: EEasingFunc * BlendExp: float * Steps: float -> Transform
    abstract TInterpTo: Target: Transform * DeltaTime: float * InterpSpeed: float -> Transform
    abstract TLerp: B: Transform * Alpha: float * InterpMode: ELerpInterpolationMode -> Transform
    abstract Transform_Determinant: unit -> float
    abstract TransformDirection: Direction: Vector -> Vector
    abstract TransformLocation: Location: Vector -> Vector
    abstract TransformRotation: Rotation: Rotator -> Rotator
    abstract K2_LookAt: TargetPosition: Vector * LookAtVector: Vector * bUseUpVector: bool * UpVector: Vector * ClampConeInDegree: float -> Transform
    abstract CalibrateExternalTrackingToHMD: unit -> unit
    abstract UpdateExternalTrackingHMDPosition: unit -> unit

type [<AllowNullLiteral>] RotatorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Rotator
    abstract C: Other: U2<UObject, obj option> -> Rotator
    abstract GetBaseRotationAndBaseOffsetInMeters: ?OutRotation: Rotator * ?OutBaseOffsetInMeters: Vector -> obj
    abstract GetBaseRotationAndPositionOffset: ?OutRot: Rotator * ?OutPosOffset: Vector -> obj
    abstract GetPose: ?DeviceRotation: Rotator * ?DevicePosition: Vector * ?NeckPosition: Vector * ?bUseOrienationForPlayerCamera: bool * ?bUsePositionForPlayerCamera: bool * ?PositionScale: Vector -> obj
    abstract SetBaseRotationAndBaseOffsetInMeters: Rotation: Rotator * BaseOffsetInMeters: Vector * Options: EOrientPositionSelector -> unit
    abstract SetBaseRotationAndPositionOffset: BaseRot: Rotator * PosOffset: Vector * Options: EOrientPositionSelector -> unit
    abstract SetBaseRotation: InBaseRotation: Rotator -> unit
    abstract Conv_RotatorToText: InRot: Rotator -> string
    abstract Conv_RotatorToString: InRot: Rotator -> string
    abstract BreakRotator: InRot: Rotator * ?Roll: float * ?Pitch: float * ?Yaw: float -> obj
    abstract BreakRotIntoAxes: InRot: Rotator * ?X: Vector * ?Y: Vector * ?Z: Vector -> obj
    abstract ComposeRotators: A: Rotator * B: Rotator -> Rotator
    abstract Conv_RotatorToTransform: InRotator: Rotator -> Transform
    abstract Conv_RotatorToVector: InRot: Rotator -> Vector
    abstract EqualEqual_RotatorRotator: A: Rotator * B: Rotator * ErrorTolerance: float -> bool
    abstract GetAxes: A: Rotator * ?X: Vector * ?Y: Vector * ?Z: Vector -> obj
    abstract GetForwardVector: InRot: Rotator -> Vector
    abstract GetRightVector: InRot: Rotator -> Vector
    abstract GetUpVector: InRot: Rotator -> Vector
    abstract Multiply_RotatorFloat: A: Rotator * B: float -> Rotator
    abstract Multiply_RotatorInt: A: Rotator * B: float -> Rotator
    abstract NegateRotator: A: Rotator -> Rotator
    abstract NormalizedDeltaRotator: A: Rotator * B: Rotator -> Rotator
    abstract NotEqual_RotatorRotator: A: Rotator * B: Rotator * ErrorTolerance: float -> bool
    abstract REase: A: Rotator * B: Rotator * Alpha: float * bShortestPath: bool * EasingFunc: EEasingFunc * BlendExp: float * Steps: float -> Rotator
    abstract RInterpTo: Current: Rotator * Target: Rotator * DeltaTime: float * InterpSpeed: float -> Rotator
    abstract RInterpTo_Constant: Current: Rotator * Target: Rotator * DeltaTime: float * InterpSpeed: float -> Rotator
    abstract RLerp: A: Rotator * B: Rotator * Alpha: float * bShortestPath: bool -> Rotator
    abstract SelectRotator: A: Rotator * B: Rotator * bPickA: bool -> Rotator
    abstract IsValidAIRotation: Rotation: Rotator -> bool
    abstract GetOrientationAndPosition: ?DeviceRotation: Rotator * ?DevicePosition: Vector -> obj
    abstract MakeRotator: Roll: float * Pitch: float * Yaw: float -> Rotator
    abstract RandomRotator: bRoll: bool -> Rotator
    abstract RandomRotatorFromStream: bRoll: bool * Stream: obj -> Rotator //RandomStream -> Rotator

type [<AllowNullLiteral>] Vector2D =
    abstract X: float with get, set
    abstract Y: float with get, set
    abstract clone: unit -> Vector2D
    abstract Conv_Vector2dToText: unit -> string
    abstract Conv_Vector2dToString: unit -> string
    abstract Add_Vector2DFloat: B: float -> Vector2D
    abstract Add_Vector2DVector2D: B: Vector2D -> Vector2D
    abstract BreakVector2D: ?X: float * ?Y: float -> obj
    abstract Conv_Vector2DToVector: Z: float -> Vector
    abstract CrossProduct2D: B: Vector2D -> float
    abstract Divide_Vector2DFloat: B: float -> Vector2D
    abstract Divide_Vector2DVector2D: B: Vector2D -> Vector2D
    abstract DotProduct2D: B: Vector2D -> float
    abstract EqualEqual_Vector2DVector2D: B: Vector2D * ErrorTolerance: float -> bool
    abstract MakeBox2D: Max: Vector2D -> Box2D
    abstract Multiply_Vector2DFloat: B: float -> Vector2D
    abstract Multiply_Vector2DVector2D: B: Vector2D -> Vector2D
    abstract Normal2D: unit -> Vector2D
    abstract NotEqual_Vector2DVector2D: B: Vector2D * ErrorTolerance: float -> bool
    abstract Subtract_Vector2DFloat: B: float -> Vector2D
    abstract Subtract_Vector2DVector2D: B: Vector2D -> Vector2D
    abstract Vector2DInterpTo: Target: Vector2D * DeltaTime: float * InterpSpeed: float -> Vector2D
    abstract Vector2DInterpTo_Constant: Target: Vector2D * DeltaTime: float * InterpSpeed: float -> Vector2D
    abstract VSize2D: unit -> float
    abstract VSize2DSquared: unit -> float
    abstract SetSpectatorScreenModeTexturePlusEyeLayout: EyeRectMax: Vector2D * TextureRectMin: Vector2D * TextureRectMax: Vector2D * bDrawEyeFirst: bool * bClearBlack: bool * bUseAlpha: bool -> unit

type [<AllowNullLiteral>] Box2D =
    abstract Min: Vector2D with get, set
    abstract Max: Vector2D with get, set
    abstract bIsValid: float with get, set
    abstract clone: unit -> Box2D

type [<AllowNullLiteral>] Box2DStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Box2D
    abstract C: Other: U2<UObject, obj option> -> Box2D

type [<AllowNullLiteral>] Vector2DStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Vector2D
    abstract C: Other: U2<UObject, obj option> -> Vector2D
    abstract Conv_Vector2dToText: InVec: Vector2D -> string
    abstract Conv_Vector2dToString: InVec: Vector2D -> string
    abstract Add_Vector2DFloat: A: Vector2D * B: float -> Vector2D
    abstract Add_Vector2DVector2D: A: Vector2D * B: Vector2D -> Vector2D
    abstract BreakVector2D: InVec: Vector2D * ?X: float * ?Y: float -> obj
    abstract Conv_Vector2DToVector: InVector2D: Vector2D * Z: float -> Vector
    abstract CrossProduct2D: A: Vector2D * B: Vector2D -> float
    abstract Divide_Vector2DFloat: A: Vector2D * B: float -> Vector2D
    abstract Divide_Vector2DVector2D: A: Vector2D * B: Vector2D -> Vector2D
    abstract DotProduct2D: A: Vector2D * B: Vector2D -> float
    abstract EqualEqual_Vector2DVector2D: A: Vector2D * B: Vector2D * ErrorTolerance: float -> bool
    abstract MakeBox2D: Min: Vector2D * Max: Vector2D -> Box2D
    abstract Multiply_Vector2DFloat: A: Vector2D * B: float -> Vector2D
    abstract Multiply_Vector2DVector2D: A: Vector2D * B: Vector2D -> Vector2D
    abstract Normal2D: A: Vector2D -> Vector2D
    abstract NotEqual_Vector2DVector2D: A: Vector2D * B: Vector2D * ErrorTolerance: float -> bool
    abstract Subtract_Vector2DFloat: A: Vector2D * B: float -> Vector2D
    abstract Subtract_Vector2DVector2D: A: Vector2D * B: Vector2D -> Vector2D
    abstract Vector2DInterpTo: Current: Vector2D * Target: Vector2D * DeltaTime: float * InterpSpeed: float -> Vector2D
    abstract Vector2DInterpTo_Constant: Current: Vector2D * Target: Vector2D * DeltaTime: float * InterpSpeed: float -> Vector2D
    abstract VSize2D: A: Vector2D -> float
    abstract VSize2DSquared: A: Vector2D -> float
    abstract SetSpectatorScreenModeTexturePlusEyeLayout: EyeRectMin: Vector2D * EyeRectMax: Vector2D * TextureRectMin: Vector2D * TextureRectMax: Vector2D * bDrawEyeFirst: bool * bClearBlack: bool * bUseAlpha: bool -> unit
    abstract GenerateDynamicImageResource: InDynamicBrushName: string -> Vector2D
    abstract MakeVector2D: X: float * Y: float -> Vector2D
    abstract NextSobolCell2D: index: float * NumCells: float * PreviousValue: Vector2D -> Vector2D
    abstract RandomSobolCell2D: index: float * NumCells: float * Cell: Vector2D * Seed: Vector2D -> Vector2D
    abstract GetMousePositionOnPlatform: unit -> Vector2D

type [<AllowNullLiteral>] Vector =
    abstract X: float with get, set
    abstract Y: float with get, set
    abstract Z: float with get, set
    abstract clone: unit -> Vector
    abstract SegmentIntersection2D: SegmentEndA: Vector * SegmentStartB: Vector * SegmentEndB: Vector * ?IntersectionPoint: Vector -> obj
    //abstract GenerateBoxMesh: ?Vertices: ResizeArray<Vector> * ?Triangles: ResizeArray<float> * ?Normals: ResizeArray<Vector> * ?UVs: ResizeArray<Vector2D> * ?Tangents: ResizeArray<ProcMeshTangent> -> obj
    //abstract GetPointGuardianIntersection: BoundaryType: EBoundaryType -> GuardianTestResult
    //abstract GetRawSensorData: ?LinearAcceleration: Vector * ?AngularVelocity: Vector * ?LinearVelocity: Vector * ?TimeInSeconds: float * ?DeviceType: ETrackedDeviceType -> obj
    abstract SetPositionScale3D: unit -> unit
    abstract SetBasePosition: unit -> unit
    abstract GetClosestARPin: ?PinID: Guid -> obj
    abstract Conv_VectorToText: unit -> string
    abstract Conv_VectorToString: unit -> string
    abstract Add_VectorFloat: B: float -> Vector
    abstract Add_VectorInt: B: float -> Vector
    abstract Add_VectorVector: B: Vector -> Vector
    abstract BreakVector: ?X: float * ?Y: float * ?Z: float -> obj
    abstract ClampVectorSize: Min: float * Max: float -> Vector
    abstract Conv_VectorToLinearColor: unit -> LinearColor
    abstract Conv_VectorToRotator: unit -> Rotator
    abstract Conv_VectorToTransform: unit -> Transform
    abstract Conv_VectorToVector2D: unit -> Vector2D
    abstract Cross_VectorVector: B: Vector -> Vector
    abstract Divide_VectorFloat: B: float -> Vector
    abstract Divide_VectorInt: B: float -> Vector
    abstract Divide_VectorVector: B: Vector -> Vector
    abstract Dot_VectorVector: B: Vector -> float
    abstract EqualEqual_VectorVector: B: Vector * ErrorTolerance: float -> bool
    abstract FindClosestPointOnLine: LineOrigin: Vector * LineDirection: Vector -> Vector
    abstract FindClosestPointOnSegment: SegmentStart: Vector * SegmentEnd: Vector -> Vector
    abstract FindLookAtRotation: Target: Vector -> Rotator
    abstract FindNearestPointsOnLineSegments: Segment1End: Vector * Segment2Start: Vector * Segment2End: Vector * ?Segment1Point: Vector * ?Segment2Point: Vector -> obj
    abstract FTruncVector: unit -> IntVector
    abstract GetAzimuthAndElevation: ReferenceFrame: Transform * ?Azimuth: float * ?Elevation: float -> obj
    abstract GetDirectionUnitVector: To: Vector -> Vector
    abstract GetMaxElement: unit -> float
    abstract GetMinElement: unit -> float
    abstract GetPointDistanceToLine: LineOrigin: Vector * LineDirection: Vector -> float
    abstract GetPointDistanceToSegment: SegmentStart: Vector * SegmentEnd: Vector -> float
    abstract GetReflectionVector: SurfaceNormal: Vector -> Vector
    abstract GetSlopeDegreeAngles: FloorNormal: Vector * UpVector: Vector * ?OutSlopePitchDegreeAngle: float * ?OutSlopeRollDegreeAngle: float -> obj
    abstract GetYawPitchFromVector: ?Yaw: float * ?Pitch: float -> obj
    abstract GreaterGreater_VectorRotator: B: Rotator -> Vector
    abstract IsPointInBox: BoxOrigin: Vector * BoxExtent: Vector -> bool
    abstract IsPointInBoxWithTransform: BoxWorldTransform: Transform * BoxExtent: Vector -> bool
    abstract LessLess_VectorRotator: B: Rotator -> Vector
    abstract LinePlaneIntersection: LineEnd: Vector * APlane: Plane * ?T: float * ?Intersection: Vector -> obj
    abstract LinePlaneIntersection_OriginNormal: LineEnd: Vector * PlaneOrigin: Vector * PlaneNormal: Vector * ?T: float * ?Intersection: Vector -> obj
    abstract MakeBox: Max: Vector -> Box
    abstract MakePlaneFromPointAndNormal: Normal: Vector -> Plane
    abstract MakeRotationFromAxes: Right: Vector * Up: Vector -> Rotator
    abstract MakeRotFromX: unit -> Rotator
    abstract MakeRotFromXY: Y: Vector -> Rotator
    abstract MakeRotFromXZ: Z: Vector -> Rotator
    abstract MakeRotFromY: unit -> Rotator
    abstract MakeRotFromYX: X: Vector -> Rotator
    abstract MakeRotFromYZ: Z: Vector -> Rotator
    abstract MakeRotFromZ: unit -> Rotator
    abstract MakeRotFromZX: X: Vector -> Rotator
    abstract MakeRotFromZY: Y: Vector -> Rotator
    abstract MakeTransform: Rotation: Rotator * Scale: Vector -> Transform
    abstract MirrorVectorByNormal: InNormal: Vector -> Vector
    abstract Multiply_VectorFloat: B: float -> Vector
    abstract Multiply_VectorInt: B: float -> Vector
    abstract Multiply_VectorVector: B: Vector -> Vector
    abstract NegateVector: unit -> Vector
    abstract Normal: unit -> Vector
    abstract NotEqual_VectorVector: B: Vector * ErrorTolerance: float -> bool
    abstract ProjectPointOnToPlane: PlaneBase: Vector * PlaneNormal: Vector -> Vector
    abstract ProjectVectorOnToPlane: PlaneNormal: Vector -> Vector
    abstract ProjectVectorOnToVector: Target: Vector -> Vector
    abstract RandomPointInBoundingBox: BoxExtent: Vector -> Vector
    abstract RandomUnitVectorInConeInDegrees: ConeHalfAngleInDegrees: float -> Vector
    //abstract RandomUnitVectorInConeInDegreesFromStream: ConeHalfAngleInDegrees: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInConeInRadians: ConeHalfAngleInRadians: float -> Vector
    //abstract RandomUnitVectorInConeInRadiansFromStream: ConeHalfAngleInRadians: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInEllipticalConeInDegrees: MaxYawInDegrees: float * MaxPitchInDegrees: float -> Vector
    //abstract RandomUnitVectorInEllipticalConeInDegreesFromStream: MaxYawInDegrees: float * MaxPitchInDegrees: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInEllipticalConeInRadians: MaxYawInRadians: float * MaxPitchInRadians: float -> Vector
    //abstract RandomUnitVectorInEllipticalConeInRadiansFromStream: MaxYawInRadians: float * MaxPitchInRadians: float * Stream: RandomStream -> Vector
    abstract RotateAngleAxis: AngleDeg: float * Axis: Vector -> Vector
    abstract RotatorFromAxisAndAngle: Angle: float -> Rotator
    abstract SelectVector: B: Vector * bPickA: bool -> Vector
    abstract Subtract_VectorFloat: B: float -> Vector
    abstract Subtract_VectorInt: B: float -> Vector
    abstract Subtract_VectorVector: B: Vector -> Vector
    abstract VEase: B: Vector * Alpha: float * EasingFunc: EEasingFunc * BlendExp: float * Steps: float -> Vector
    abstract VectorSpringInterp: Target: Vector * ?SpringState: VectorSpringState * ?Stiffness: float * ?CriticalDampingFactor: float * ?DeltaTime: float * ?Mass: float -> obj
    abstract VInterpTo: Target: Vector * DeltaTime: float * InterpSpeed: float -> Vector
    abstract VInterpTo_Constant: Target: Vector * DeltaTime: float * InterpSpeed: float -> Vector
    abstract VLerp: B: Vector * Alpha: float -> Vector
    abstract VSize: unit -> float
    abstract VSizeSquared: unit -> float
    abstract VSizeXY: unit -> float
    abstract K2_TwoBoneIK: JointPos: Vector * EndPos: Vector * JointTarget: Vector * Effector: Vector * ?OutJointPos: Vector * ?OutEndPos: Vector * ?bAllowStretching: bool * ?StartStretchRatio: float * ?MaxStretchScale: float -> obj
    abstract IsValidAIDirection: unit -> bool
    abstract IsValidAILocation: unit -> bool
    abstract GetPositionalTrackingCameraParameters: ?CameraRotation: Rotator * ?HFOV: float * ?VFOV: float * ?CameraDistance: float * ?NearPlane: float * ?FarPlane: float -> obj
    abstract GetTrackingSensorParameters: ?Rotation: Rotator * ?LeftFOV: float * ?RightFOV: float * ?TopFOV: float * ?BottomFOV: float * ?Distance: float * ?NearPlane: float * ?FarPlane: float * ?IsActive: bool * ?index: float -> obj

type [<AllowNullLiteral>] VectorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Vector
    abstract C: Other: U2<UObject, obj option> -> Vector
    //abstract SegmentIntersection2D: SegmentStartA: Vector * SegmentEndA: Vector * SegmentStartB: Vector * SegmentEndB: Vector * ?IntersectionPoint: Vector -> obj
    //abstract GenerateBoxMesh: BoxRadius: Vector * ?Vertices: ResizeArray<Vector> * ?Triangles: ResizeArray<float> * ?Normals: ResizeArray<Vector> * ?UVs: ResizeArray<Vector2D> * ?Tangents: ResizeArray<ProcMeshTangent> -> obj
    //abstract GetPointGuardianIntersection: Point: Vector * BoundaryType: EBoundaryType -> GuardianTestResult
    //abstract GetRawSensorData: ?AngularAcceleration: Vector * ?LinearAcceleration: Vector * ?AngularVelocity: Vector * ?LinearVelocity: Vector * ?TimeInSeconds: float * ?DeviceType: ETrackedDeviceType -> obj
    abstract SetPositionScale3D: PosScale3D: Vector -> unit
    abstract SetBasePosition: InBasePosition: Vector -> unit
    abstract GetClosestARPin: SearchPoint: Vector * ?PinID: Guid -> obj
    abstract Conv_VectorToText: InVec: Vector -> string
    abstract Conv_VectorToString: InVec: Vector -> string
    abstract Add_VectorFloat: A: Vector * B: float -> Vector
    abstract Add_VectorInt: A: Vector * B: float -> Vector
    abstract Add_VectorVector: A: Vector * B: Vector -> Vector
    abstract BreakVector: InVec: Vector * ?X: float * ?Y: float * ?Z: float -> obj
    abstract ClampVectorSize: A: Vector * Min: float * Max: float -> Vector
    abstract Conv_VectorToLinearColor: InVec: Vector -> LinearColor
    abstract Conv_VectorToRotator: InVec: Vector -> Rotator
    abstract Conv_VectorToTransform: InLocation: Vector -> Transform
    abstract Conv_VectorToVector2D: InVector: Vector -> Vector2D
    abstract Cross_VectorVector: A: Vector * B: Vector -> Vector
    abstract Divide_VectorFloat: A: Vector * B: float -> Vector
    abstract Divide_VectorInt: A: Vector * B: float -> Vector
    abstract Divide_VectorVector: A: Vector * B: Vector -> Vector
    abstract Dot_VectorVector: A: Vector * B: Vector -> float
    abstract EqualEqual_VectorVector: A: Vector * B: Vector * ErrorTolerance: float -> bool
    abstract FindClosestPointOnLine: Point: Vector * LineOrigin: Vector * LineDirection: Vector -> Vector
    abstract FindClosestPointOnSegment: Point: Vector * SegmentStart: Vector * SegmentEnd: Vector -> Vector
    abstract FindLookAtRotation: Start: Vector * Target: Vector -> Rotator
    abstract FindNearestPointsOnLineSegments: Segment1Start: Vector * Segment1End: Vector * Segment2Start: Vector * Segment2End: Vector * ?Segment1Point: Vector * ?Segment2Point: Vector -> obj
    abstract FTruncVector: InVector: Vector -> IntVector
    abstract GetAzimuthAndElevation: InDirection: Vector * ReferenceFrame: Transform * ?Azimuth: float * ?Elevation: float -> obj
    abstract GetDirectionUnitVector: From: Vector * To: Vector -> Vector
    abstract GetMaxElement: A: Vector -> float
    abstract GetMinElement: A: Vector -> float
    abstract GetPointDistanceToLine: Point: Vector * LineOrigin: Vector * LineDirection: Vector -> float
    abstract GetPointDistanceToSegment: Point: Vector * SegmentStart: Vector * SegmentEnd: Vector -> float
    abstract GetReflectionVector: Direction: Vector * SurfaceNormal: Vector -> Vector
    abstract GetSlopeDegreeAngles: MyRightYAxis: Vector * FloorNormal: Vector * UpVector: Vector * ?OutSlopePitchDegreeAngle: float * ?OutSlopeRollDegreeAngle: float -> obj
    abstract GetYawPitchFromVector: InVec: Vector * ?Yaw: float * ?Pitch: float -> obj
    abstract GreaterGreater_VectorRotator: A: Vector * B: Rotator -> Vector
    abstract IsPointInBox: Point: Vector * BoxOrigin: Vector * BoxExtent: Vector -> bool
    abstract IsPointInBoxWithTransform: Point: Vector * BoxWorldTransform: Transform * BoxExtent: Vector -> bool
    abstract LessLess_VectorRotator: A: Vector * B: Rotator -> Vector
    abstract LinePlaneIntersection: LineStart: Vector * LineEnd: Vector * APlane: Plane * ?T: float * ?Intersection: Vector -> obj
    abstract LinePlaneIntersection_OriginNormal: LineStart: Vector * LineEnd: Vector * PlaneOrigin: Vector * PlaneNormal: Vector * ?T: float * ?Intersection: Vector -> obj
    abstract MakeBox: Min: Vector * Max: Vector -> Box
    abstract MakePlaneFromPointAndNormal: Point: Vector * Normal: Vector -> Plane
    abstract MakeRotationFromAxes: Forward: Vector * Right: Vector * Up: Vector -> Rotator
    abstract MakeRotFromX: X: Vector -> Rotator
    abstract MakeRotFromXY: X: Vector * Y: Vector -> Rotator
    abstract MakeRotFromXZ: X: Vector * Z: Vector -> Rotator
    abstract MakeRotFromY: Y: Vector -> Rotator
    abstract MakeRotFromYX: Y: Vector * X: Vector -> Rotator
    abstract MakeRotFromYZ: Y: Vector * Z: Vector -> Rotator
    abstract MakeRotFromZ: Z: Vector -> Rotator
    abstract MakeRotFromZX: Z: Vector * X: Vector -> Rotator
    abstract MakeRotFromZY: Z: Vector * Y: Vector -> Rotator
    abstract MakeTransform: Location: Vector * Rotation: Rotator * Scale: Vector -> Transform
    abstract MirrorVectorByNormal: InVect: Vector * InNormal: Vector -> Vector
    abstract Multiply_VectorFloat: A: Vector * B: float -> Vector
    abstract Multiply_VectorInt: A: Vector * B: float -> Vector
    abstract Multiply_VectorVector: A: Vector * B: Vector -> Vector
    abstract NegateVector: A: Vector -> Vector
    abstract Normal: A: Vector -> Vector
    abstract NotEqual_VectorVector: A: Vector * B: Vector * ErrorTolerance: float -> bool
    abstract ProjectPointOnToPlane: Point: Vector * PlaneBase: Vector * PlaneNormal: Vector -> Vector
    abstract ProjectVectorOnToPlane: V: Vector * PlaneNormal: Vector -> Vector
    abstract ProjectVectorOnToVector: V: Vector * Target: Vector -> Vector
    abstract RandomPointInBoundingBox: Origin: Vector * BoxExtent: Vector -> Vector
    abstract RandomUnitVectorInConeInDegrees: ConeDir: Vector * ConeHalfAngleInDegrees: float -> Vector
    //abstract RandomUnitVectorInConeInDegreesFromStream: ConeDir: Vector * ConeHalfAngleInDegrees: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInConeInRadians: ConeDir: Vector * ConeHalfAngleInRadians: float -> Vector
    //abstract RandomUnitVectorInConeInRadiansFromStream: ConeDir: Vector * ConeHalfAngleInRadians: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInEllipticalConeInDegrees: ConeDir: Vector * MaxYawInDegrees: float * MaxPitchInDegrees: float -> Vector
    //abstract RandomUnitVectorInEllipticalConeInDegreesFromStream: ConeDir: Vector * MaxYawInDegrees: float * MaxPitchInDegrees: float * Stream: RandomStream -> Vector
    abstract RandomUnitVectorInEllipticalConeInRadians: ConeDir: Vector * MaxYawInRadians: float * MaxPitchInRadians: float -> Vector
    //abstract RandomUnitVectorInEllipticalConeInRadiansFromStream: ConeDir: Vector * MaxYawInRadians: float * MaxPitchInRadians: float * Stream: RandomStream -> Vector
    abstract RotateAngleAxis: InVect: Vector * AngleDeg: float * Axis: Vector -> Vector
    abstract RotatorFromAxisAndAngle: Axis: Vector * Angle: float -> Rotator
    abstract SelectVector: A: Vector * B: Vector * bPickA: bool -> Vector
    abstract Subtract_VectorFloat: A: Vector * B: float -> Vector
    abstract Subtract_VectorInt: A: Vector * B: float -> Vector
    abstract Subtract_VectorVector: A: Vector * B: Vector -> Vector
    abstract VEase: A: Vector * B: Vector * Alpha: float * EasingFunc: EEasingFunc * BlendExp: float * Steps: float -> Vector
    abstract VectorSpringInterp: Current: Vector * Target: Vector * ?SpringState: VectorSpringState * ?Stiffness: float * ?CriticalDampingFactor: float * ?DeltaTime: float * ?Mass: float -> obj
    abstract VInterpTo: Current: Vector * Target: Vector * DeltaTime: float * InterpSpeed: float -> Vector
    abstract VInterpTo_Constant: Current: Vector * Target: Vector * DeltaTime: float * InterpSpeed: float -> Vector
    abstract VLerp: A: Vector * B: Vector * Alpha: float -> Vector
    abstract VSize: A: Vector -> float
    abstract VSizeSquared: A: Vector -> float
    abstract VSizeXY: A: Vector -> float
    abstract K2_TwoBoneIK: RootPos: Vector * JointPos: Vector * EndPos: Vector * JointTarget: Vector * Effector: Vector * ?OutJointPos: Vector * ?OutEndPos: Vector * ?bAllowStretching: bool * ?StartStretchRatio: float * ?MaxStretchScale: float -> obj
    abstract IsValidAIDirection: DirectionVector: Vector -> bool
    abstract IsValidAILocation: Location: Vector -> bool
    abstract GetPositionalTrackingCameraParameters: ?CameraOrigin: Vector * ?CameraRotation: Rotator * ?HFOV: float * ?VFOV: float * ?CameraDistance: float * ?NearPlane: float * ?FarPlane: float -> obj
    abstract GetTrackingSensorParameters: ?Origin: Vector * ?Rotation: Rotator * ?LeftFOV: float * ?RightFOV: float * ?TopFOV: float * ?BottomFOV: float * ?Distance: float * ?NearPlane: float * ?FarPlane: float * ?IsActive: bool * ?index: float -> obj
    //abstract GetGuardianDimensions: BoundaryType: EBoundaryType -> Vector
    abstract Conv_FloatToVector: InFloat: float -> Vector
    abstract CreateVectorFromYawPitch: Yaw: float * Pitch: float * Length: float -> Vector
    abstract GetVectorArrayAverage: Vectors: ResizeArray<Vector> -> Vector
    abstract MakeVector: X: float * Y: float * Z: float -> Vector
    abstract RandomUnitVector: unit -> Vector
    abstract NextSobolCell3D: index: float * NumCells: float * PreviousValue: Vector -> Vector
    abstract RandomSobolCell3D: index: float * NumCells: float * Cell: Vector * Seed: Vector -> Vector
    abstract GetActorArrayAverageLocation: Actors: ResizeArray<Actor> -> Vector
    abstract K2_MakePerlinNoiseVectorAndRemap: X: float * Y: float * Z: float * RangeOutMinX: float * RangeOutMaxX: float * RangeOutMinY: float * RangeOutMaxY: float * RangeOutMinZ: float * RangeOutMaxZ: float -> Vector

type [<AllowNullLiteral>] Guid =
    abstract A: float with get, set
    abstract B: float with get, set
    abstract C: float with get, set
    abstract D: float with get, set
    abstract clone: unit -> Guid
    abstract GetARPinPositionAndOrientation: ?Position: Vector * ?Orientation: Rotator * ?PinFoundInEnvironment: bool -> obj
    abstract Conv_GuidToString: unit -> string
    abstract EqualEqual_GuidGuid: B: Guid -> bool
    abstract Invalidate_Guid: unit -> obj
    abstract IsValid_Guid: unit -> bool
    abstract NotEqual_GuidGuid: B: Guid -> bool

type [<AllowNullLiteral>] GuidStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Guid
    abstract C: Other: U2<UObject, obj option> -> Guid
    abstract GetARPinPositionAndOrientation: PinID: Guid * ?Position: Vector * ?Orientation: Rotator * ?PinFoundInEnvironment: bool -> obj
    abstract Conv_GuidToString: InGuid: Guid -> string
    abstract EqualEqual_GuidGuid: A: Guid * B: Guid -> bool
    abstract Invalidate_Guid: ?InGuid: Guid -> obj
    abstract IsValid_Guid: InGuid: Guid -> bool
    abstract NotEqual_GuidGuid: A: Guid * B: Guid -> bool
    abstract NewGuid: unit -> Guid

type [<StringEnum>] [<RequireQualifiedAccess>] EPassableWorldError =
    | [<CompiledName "None">] None
    | [<CompiledName "LowMapQuality">] LowMapQuality
    | [<CompiledName "UnableToLocalize">] UnableToLocalize
    | [<CompiledName "Unavailable">] Unavailable
    | [<CompiledName "PrivilegeDenied">] PrivilegeDenied
    | [<CompiledName "InvalidParam">] InvalidParam
    | [<CompiledName "UnspecifiedFailure">] UnspecifiedFailure
    | [<CompiledName "PrivilegeRequestPending">] PrivilegeRequestPending
    | [<CompiledName "EPassableWorldError_MAX">] EPassableWorldError_MAX

type [<AllowNullLiteral>] Color =
    abstract B: float with get, set
    abstract G: float with get, set
    abstract R: float with get, set
    abstract A: float with get, set
    abstract clone: unit -> Color
    abstract Conv_ColorToLinearColor: unit -> LinearColor

type [<AllowNullLiteral>] ColorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Color
    abstract C: Other: U2<UObject, obj option> -> Color
    abstract Conv_ColorToLinearColor: InColor: Color -> LinearColor

type [<AllowNullLiteral>] LinearColor =
    abstract R: float with get, set
    abstract G: float with get, set
    abstract B: float with get, set
    abstract A: float with get, set
    abstract clone: unit -> LinearColor
    abstract Conv_ColorToText: unit -> string
    abstract Conv_ColorToString: unit -> string
    abstract BreakColor: ?R: float * ?G: float * ?B: float * ?A: float -> obj
    abstract CInterpTo: Target: LinearColor * DeltaTime: float * InterpSpeed: float -> LinearColor
    abstract Conv_LinearColorToColor: unit -> Color
    abstract Conv_LinearColorToVector: unit -> Vector
    abstract HSVToRGB_Vector: ?RGB: LinearColor -> obj
    abstract LinearColorLerp: B: LinearColor * Alpha: float -> LinearColor
    abstract LinearColorLerpUsingHSV: B: LinearColor * Alpha: float -> LinearColor
    abstract Multiply_LinearColorFloat: B: float -> LinearColor
    abstract Multiply_LinearColorLinearColor: B: LinearColor -> LinearColor
    abstract RGBToHSV: ?H: float * ?S: float * ?V: float * ?A: float -> obj
    abstract RGBToHSV_Vector: ?HSV: LinearColor -> obj
    abstract SelectColor: B: LinearColor * bPickA: bool -> LinearColor

type [<AllowNullLiteral>] LinearColorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> LinearColor
    abstract C: Other: U2<UObject, obj option> -> LinearColor
    abstract Conv_ColorToText: InColor: LinearColor -> string
    abstract Conv_ColorToString: InColor: LinearColor -> string
    abstract BreakColor: InColor: LinearColor * ?R: float * ?G: float * ?B: float * ?A: float -> obj
    abstract CInterpTo: Current: LinearColor * Target: LinearColor * DeltaTime: float * InterpSpeed: float -> LinearColor
    abstract Conv_LinearColorToColor: InLinearColor: LinearColor -> Color
    abstract Conv_LinearColorToVector: InLinearColor: LinearColor -> Vector
    abstract HSVToRGB_Vector: HSV: LinearColor * ?RGB: LinearColor -> obj
    abstract LinearColorLerp: A: LinearColor * B: LinearColor * Alpha: float -> LinearColor
    abstract LinearColorLerpUsingHSV: A: LinearColor * B: LinearColor * Alpha: float -> LinearColor
    abstract Multiply_LinearColorFloat: A: LinearColor * B: float -> LinearColor
    abstract Multiply_LinearColorLinearColor: A: LinearColor * B: LinearColor -> LinearColor
    abstract RGBToHSV: InColor: LinearColor * ?H: float * ?S: float * ?V: float * ?A: float -> obj
    abstract RGBToHSV_Vector: RGB: LinearColor * ?HSV: LinearColor -> obj
    abstract SelectColor: A: LinearColor * B: LinearColor * bPickA: bool -> LinearColor
    abstract Conv_FloatToLinearColor: InFloat: float -> LinearColor
    abstract HSVToRGB: H: float * S: float * V: float * A: float -> LinearColor
    abstract MakeColor: R: float * G: float * B: float * A: float -> LinearColor

type [<AllowNullLiteral>] IntVector =
    abstract X: float with get, set
    abstract Y: float with get, set
    abstract Z: float with get, set
    abstract clone: unit -> IntVector
    abstract Conv_IntVectorToString: unit -> string
    abstract Conv_IntVectorToVector: unit -> Vector

type [<AllowNullLiteral>] IntVectorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> IntVector
    abstract C: Other: U2<UObject, obj option> -> IntVector
    abstract Conv_IntVectorToString: InIntVec: IntVector -> string
    abstract Conv_IntVectorToVector: InIntVector: IntVector -> Vector
    abstract Conv_IntToIntVector: inInt: float -> IntVector

type [<AllowNullLiteral>] Plane =
    inherit Vector
    abstract W: float with get, set
    abstract clone: unit -> Plane

type [<AllowNullLiteral>] PlaneStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Plane
    abstract C: Other: U2<UObject, obj option> -> Plane

type [<AllowNullLiteral>] Box =
    abstract Min: Vector with get, set
    abstract Max: Vector with get, set
    abstract IsValid: float with get, set
    abstract clone: unit -> Box

type [<AllowNullLiteral>] BoxStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Box
    abstract C: Other: U2<UObject, obj option> -> Box

type [<AllowNullLiteral>] VectorSpringState =
    abstract clone: unit -> VectorSpringState
    abstract ResetVectorSpringState: unit -> obj

type [<AllowNullLiteral>] VectorSpringStateStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> VectorSpringState
    abstract C: Other: U2<UObject, obj option> -> VectorSpringState
    abstract ResetVectorSpringState: ?SpringState: VectorSpringState -> obj

type [<AllowNullLiteral>] Actor =
    inherit UObject

type [<AllowNullLiteral>] TextPropertyTestObject =
    inherit UObject
    abstract DefaultedText: string with get, set
    abstract UndefaultedText: string with get, set
    abstract TransientText: string with get, set

type [<AllowNullLiteral>] TextPropertyTestObjectStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> TextPropertyTestObject
    abstract Load: ResourceName: string -> TextPropertyTestObject
    abstract Find: Outer: UObject * ResourceName: string -> TextPropertyTestObject
    abstract GetDefaultObject: unit -> TextPropertyTestObject
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> TextPropertyTestObject
    abstract C: Other: U2<UObject, obj option> -> TextPropertyTestObject

type [<AllowNullLiteral>] MaterialInterface =
    inherit UObject
    //abstract SubsurfaceProfile: SubsurfaceProfile with get, set
    //abstract LightmassSettings: LightmassMaterialInterfaceSettings with get, set
    abstract bTextureStreamingDataSorted: bool with get, set
    abstract TextureStreamingDataVersion: float with get, set
    //abstract TextureStreamingData: ResizeArray<MaterialTextureInfo> with get, set
    //abstract AssetUserData: ResizeArray<AssetUserData> with get, set
    //abstract PreviewMesh: SoftObjectPath with get, set
    //abstract ThumbnailInfo: ThumbnailInfo with get, set
    abstract LayerParameterExpansion: obj option with get, set
    //abstract AssetImportData: AssetImportData with get, set
    abstract LightingGuid: Guid with get, set
    abstract SetForceMipLevelsToBeResident: OverrideForceMiplevelsToBeResident: bool * bForceMiplevelsToBeResidentValue: bool * ForceDuration: float * CinematicTextureGroups: float -> unit
    //abstract GetPhysicalMaterial: unit -> PhysicalMaterial
    //abstract GetBaseMaterial: unit -> Material
    //abstract SpawnDecalAttached: DecalSize: Vector * AttachToComponent: SceneComponent * AttachPointName: string * Location: Vector * Rotation: Rotator * LocationType: EAttachLocation * LifeSpan: float -> DecalComponent
    //abstract MakeBrushFromMaterial: Width: float * Height: float -> SlateBrush

type [<AllowNullLiteral>] MaterialInterfaceStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> MaterialInterface
    abstract Load: ResourceName: string -> MaterialInterface
    abstract Find: Outer: UObject * ResourceName: string -> MaterialInterface
    abstract GetDefaultObject: unit -> MaterialInterface
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> MaterialInterface
    abstract C: Other: U2<UObject, obj option> -> MaterialInterface
    //abstract SpawnDecalAttached: DecalMaterial: MaterialInterface * DecalSize: Vector * AttachToComponent: SceneComponent * AttachPointName: string * Location: Vector * Rotation: Rotator * LocationType: EAttachLocation * LifeSpan: float -> DecalComponent
    //abstract MakeBrushFromMaterial: Material: MaterialInterface * Width: float * Height: float -> SlateBrush

type [<StringEnum>] [<RequireQualifiedAccess>] ERadialImpulseFalloff =
    | [<CompiledName "RIF_Constant">] RIF_Constant
    | [<CompiledName "RIF_Linear">] RIF_Linear
    | [<CompiledName "RIF_MAX">] RIF_MAX

type [<StringEnum>] [<RequireQualifiedAccess>] EDetachmentRule =
    | [<CompiledName "KeepRelative">] KeepRelative
    | [<CompiledName "KeepWorld">] KeepWorld
    | [<CompiledName "EDetachmentRule_MAX">] EDetachmentRule_MAX

type [<StringEnum>] [<RequireQualifiedAccess>] EMoveComponentAction =
    | [<CompiledName "Move">] Move
    | [<CompiledName "Stop">] Stop
    | [<CompiledName "Return">] Return
    | [<CompiledName "EMoveComponentAction_MAX">] EMoveComponentAction_MAX

type [<AllowNullLiteral>] ActorComponent =
    inherit UObject
    
type [<AllowNullLiteral>] SceneComponent =
    inherit ActorComponent
    abstract PhysicsVolume: obj option with get, set
    abstract AttachParent: SceneComponent with get, set
    abstract AttachSocketName: string with get, set
    abstract AttachChildren: ResizeArray<SceneComponent> with get, set
    abstract ClientAttachedChildren: ResizeArray<SceneComponent> with get, set
    abstract RelativeLocation: Vector with get, set
    abstract RelativeRotation: Rotator with get, set
    abstract RelativeScale3D: Vector with get, set
    abstract ComponentVelocity: Vector with get, set
    abstract bComponentToWorldUpdated: bool with get, set
    abstract bAbsoluteLocation: bool with get, set
    abstract bAbsoluteRotation: bool with get, set
    abstract bAbsoluteScale: bool with get, set
    abstract bVisible: bool with get, set
    abstract bHiddenInGame: bool with get, set
    abstract bShouldUpdatePhysicsVolume: bool with get, set
    abstract bBoundsChangeTriggersStreamingDataRebuild: bool with get, set
    abstract bUseAttachParentBound: bool with get, set
    abstract bAbsoluteTranslation: bool with get, set
    abstract bVisualizeComponent: bool with get, set
    //abstract Mobility: EComponentMobility with get, set
    //abstract DetailMode: EDetailMode with get, set
    //abstract PhysicsVolumeChangedDelegate: UnrealEngineMulticastDelegate<(PhysicsVolume -> unit)> with get, set
    abstract RelativeTranslation: Vector with get, set
    abstract ToggleVisibility: bPropagateToChildren: bool -> unit
    abstract SnapTo: InParent: SceneComponent * InSocketName: string -> bool
    abstract SetWorldScale3D: NewScale: Vector -> unit
    abstract SetVisibility: bNewVisibility: bool * bPropagateToChildren: bool -> unit
    abstract SetShouldUpdatePhysicsVolume: bInShouldUpdatePhysicsVolume: bool -> unit
    abstract SetRelativeScale3D: NewScale3D: Vector -> unit
    //abstract SetMobility: NewMobility: EComponentMobility -> unit
    abstract SetHiddenInGame: NewHidden: bool * bPropagateToChildren: bool -> unit
    abstract SetAbsolute: bNewAbsoluteLocation: bool * bNewAbsoluteRotation: bool * bNewAbsoluteScale: bool -> unit
    abstract ResetRelativeTransform: unit -> unit
    abstract OnRep_Visibility: OldValue: bool -> unit
    abstract OnRep_Transform: unit -> unit
    abstract OnRep_AttachSocketName: unit -> unit
    abstract OnRep_AttachParent: unit -> unit
    abstract OnRep_AttachChildren: unit -> unit
    //abstract K2_SetWorldTransform: NewTransform: Transform * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetWorldRotation: NewRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetWorldLocationAndRotation: NewLocation: Vector * NewRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetWorldLocation: NewLocation: Vector * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetRelativeTransform: NewTransform: Transform * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetRelativeRotation: NewRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetRelativeLocationAndRotation: NewLocation: Vector * NewRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_SetRelativeLocation: NewLocation: Vector * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    abstract K2_GetComponentToWorld: unit -> Transform
    abstract K2_GetComponentScale: unit -> Vector
    abstract K2_GetComponentRotation: unit -> Rotator
    abstract K2_GetComponentLocation: unit -> Vector
    abstract K2_DetachFromComponent: LocationRule: EDetachmentRule * RotationRule: EDetachmentRule * ScaleRule: EDetachmentRule * bCallModify: bool -> unit
    //abstract K2_AttachToComponent: Parent: SceneComponent * SocketName: string * LocationRule: EAttachmentRule * RotationRule: EAttachmentRule * ScaleRule: EAttachmentRule * bWeldSimulatedBodies: bool -> bool
    //abstract K2_AttachTo: InParent: SceneComponent * InSocketName: string * AttachType: EAttachLocation * bWeldSimulatedBodies: bool -> bool
    //abstract K2_AddWorldTransform: DeltaTransform: Transform * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddWorldRotation: DeltaRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddWorldOffset: DeltaLocation: Vector * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddRelativeRotation: DeltaRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddRelativeLocation: DeltaLocation: Vector * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddLocalTransform: DeltaTransform: Transform * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddLocalRotation: DeltaRotation: Rotator * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    //abstract K2_AddLocalOffset: DeltaLocation: Vector * bSweep: bool * ?SweepHitResult: HitResult * ?bTeleport: bool -> obj
    abstract IsVisible: unit -> bool
    abstract IsSimulatingPhysics: BoneName: string -> bool
    abstract IsAnySimulatingPhysics: unit -> bool
    abstract GetUpVector: unit -> Vector
    //abstract GetSocketTransform: InSocketName: string * TransformSpace: ERelativeTransformSpace -> Transform
    abstract GetSocketRotation: InSocketName: string -> Rotator
    abstract GetSocketQuaternion: InSocketName: string -> Quat
    abstract GetSocketLocation: InSocketName: string -> Vector
    abstract GetShouldUpdatePhysicsVolume: unit -> bool
    abstract GetRightVector: unit -> Vector
    abstract GetRelativeTransform: unit -> Transform
    //abstract GetPhysicsVolume: unit -> PhysicsVolume
    abstract GetParentComponents: ?Parents: ResizeArray<SceneComponent> -> obj
    abstract GetNumChildrenComponents: unit -> float
    abstract GetForwardVector: unit -> Vector
    abstract GetComponentVelocity: unit -> Vector
    abstract GetChildrenComponents: bIncludeAllDescendants: bool * ?Children: ResizeArray<SceneComponent> -> obj
    abstract GetChildComponent: ChildIndex: float -> SceneComponent
    abstract GetAttachSocketName: unit -> string
    abstract GetAttachParent: unit -> SceneComponent
    abstract GetAllSocketNames: unit -> ResizeArray<string>
    abstract DoesSocketExist: InSocketName: string -> bool
    abstract DetachFromParent: bMaintainWorldPosition: bool * bCallModify: bool -> unit
    abstract SetMobile: unit -> unit
    //abstract SetMobility: Type: EComponentMobility -> unit
    abstract GetComponentBounds: ?Origin: Vector * ?BoxExtent: Vector * ?SphereRadius: float -> obj
    //abstract MoveComponentTo: TargetRelativeLocation: Vector * TargetRelativeRotation: Rotator * bEaseOut: bool * bEaseIn: bool * OverTime: float * bForceShortestRotationPath: bool * MoveAction: EMoveComponentAction * LatentInfo: LatentActionInfo -> unit

type [<AllowNullLiteral>] SceneComponentStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> SceneComponent
    abstract Load: ResourceName: string -> SceneComponent
    abstract Find: Outer: UObject * ResourceName: string -> SceneComponent
    abstract GetDefaultObject: unit -> SceneComponent
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> SceneComponent
    abstract C: Other: U2<UObject, obj option> -> SceneComponent
    abstract SetMobile: SceneComponent: SceneComponent -> unit
    //abstract SetMobility: SceneComponent: SceneComponent * Type: EComponentMobility -> unit
    abstract GetComponentBounds: Component: SceneComponent * ?Origin: Vector * ?BoxExtent: Vector * ?SphereRadius: float -> obj
    //abstract MoveComponentTo: Component: SceneComponent * TargetRelativeLocation: Vector * TargetRelativeRotation: Rotator * bEaseOut: bool * bEaseIn: bool * OverTime: float * bForceShortestRotationPath: bool * MoveAction: EMoveComponentAction * LatentInfo: LatentActionInfo -> unit


type [<AllowNullLiteral>] PrimitiveComponent =
    inherit SceneComponent
    abstract MinDrawDistance: float with get, set
    abstract LDMaxDrawDistance: float with get, set
    abstract CachedMaxDrawDistance: float with get, set
    //abstract DepthPriorityGroup: ESceneDepthPriorityGroup with get, set
    //abstract ViewOwnerDepthPriorityGroup: ESceneDepthPriorityGroup with get, set
    //abstract IndirectLightingCacheQuality: EIndirectLightingCacheQuality with get, set
    //abstract LightmapType: ELightmapType with get, set
    abstract bEnableAutoLODGeneration: bool with get, set
    abstract bUseMaxLODAsImposter: bool with get, set
    abstract ExcludeForSpecificHLODLevels: ResizeArray<float> with get, set
    abstract bNeverDistanceCull: bool with get, set
    abstract bAlwaysCreatePhysicsState: bool with get, set
    abstract bGenerateOverlapEvents: bool with get, set
    abstract bMultiBodyOverlap: bool with get, set
    abstract bCheckAsyncSceneOnMove: bool with get, set
    abstract bTraceComplexOnMove: bool with get, set
    abstract bReturnMaterialOnMove: bool with get, set
    abstract bUseViewOwnerDepthPriorityGroup: bool with get, set
    abstract bAllowCullDistanceVolume: bool with get, set
    abstract bHasMotionBlurVelocityMeshes: bool with get, set
    abstract bVisibleInReflectionCaptures: bool with get, set
    abstract bRenderInMainPass: bool with get, set
    abstract bRenderInMono: bool with get, set
    abstract bReceivesDecals: bool with get, set
    abstract bOwnerNoSee: bool with get, set
    abstract bOnlyOwnerSee: bool with get, set
    abstract bTreatAsBackgroundForOcclusion: bool with get, set
    abstract bUseAsOccluder: bool with get, set
    abstract bSelectable: bool with get, set
    abstract bForceMipStreaming: bool with get, set
    abstract bHasPerInstanceHitProxies: bool with get, set
    abstract CastShadow: bool with get, set
    abstract bAffectDynamicIndirectLighting: bool with get, set
    abstract bAffectDistanceFieldLighting: bool with get, set
    abstract bCastDynamicShadow: bool with get, set
    abstract bCastStaticShadow: bool with get, set
    abstract bCastVolumetricTranslucentShadow: bool with get, set
    abstract bSelfShadowOnly: bool with get, set
    abstract bCastFarShadow: bool with get, set
    abstract bCastInsetShadow: bool with get, set
    abstract bCastCinematicShadow: bool with get, set
    abstract bCastHiddenShadow: bool with get, set
    abstract bCastShadowAsTwoSided: bool with get, set
    abstract bLightAsIfStatic: bool with get, set
    abstract bLightAttachmentsAsGroup: bool with get, set
    abstract bReceiveMobileCSMShadows: bool with get, set
    abstract bSingleSampleShadowFromStationaryLights: bool with get, set
    abstract bIgnoreRadialImpulse: bool with get, set
    abstract bIgnoreRadialForce: bool with get, set
    abstract bApplyImpulseOnDamage: bool with get, set
    abstract bReplicatePhysicsToAutonomousProxy: bool with get, set
    abstract AlwaysLoadOnClient: bool with get, set
    abstract AlwaysLoadOnServer: bool with get, set
    abstract bUseEditorCompositing: bool with get, set
    abstract bRenderCustomDepth: bool with get, set
    //abstract bHasCustomNavigableGeometry: EHasCustomNavigableGeometry with get, set
    //abstract CanBeCharacterBase: ECanBeCharacterBase with get, set
    //abstract CanCharacterStepUpOn: ECanBeCharacterBase with get, set
    //abstract LightingChannels: LightingChannels with get, set
    //abstract CustomDepthStencilWriteMask: ERendererStencilMask with get, set
    abstract CustomDepthStencilValue: float with get, set
    abstract TranslucencySortPriority: float with get, set
    abstract VisibilityId: float with get, set
    abstract LpvBiasMultiplier: float with get, set
    abstract BoundsScale: float with get, set
    abstract LastSubmitTime: float with get, set
    abstract LastRenderTime: float with get, set
    abstract LastRenderTimeOnScreen: float with get, set
    abstract MoveIgnoreActors: ResizeArray<Actor> with get, set
    abstract MoveIgnoreComponents: ResizeArray<PrimitiveComponent> with get, set
    //abstract BodyInstance: BodyInstance with get, set
    //abstract OnComponentHit: UnrealEngineMulticastDelegate<(PrimitiveComponent -> Actor -> PrimitiveComponent -> Vector -> HitResult -> unit)> with get, set
    //abstract OnComponentBeginOverlap: UnrealEngineMulticastDelegate<(PrimitiveComponent -> Actor -> PrimitiveComponent -> float -> bool -> HitResult -> unit)> with get, set
    //abstract OnComponentEndOverlap: UnrealEngineMulticastDelegate<(PrimitiveComponent -> Actor -> PrimitiveComponent -> float -> unit)> with get, set
    //abstract OnComponentWake: UnrealEngineMulticastDelegate<(PrimitiveComponent -> string -> unit)> with get, set
    //abstract OnComponentSleep: UnrealEngineMulticastDelegate<(PrimitiveComponent -> string -> unit)> with get, set
    //abstract OnBeginCursorOver: UnrealEngineMulticastDelegate<(PrimitiveComponent -> unit)> with get, set
    //abstract OnEndCursorOver: UnrealEngineMulticastDelegate<(PrimitiveComponent -> unit)> with get, set
    //abstract OnClicked: UnrealEngineMulticastDelegate<(PrimitiveComponent -> Key -> unit)> with get, set
    //abstract OnReleased: UnrealEngineMulticastDelegate<(PrimitiveComponent -> Key -> unit)> with get, set
    //abstract OnInputTouchBegin: UnrealEngineMulticastDelegate<(ETouchIndex -> PrimitiveComponent -> unit)> with get, set
    //abstract OnInputTouchEnd: UnrealEngineMulticastDelegate<(ETouchIndex -> PrimitiveComponent -> unit)> with get, set
    //abstract OnInputTouchEnter: UnrealEngineMulticastDelegate<(ETouchIndex -> PrimitiveComponent -> unit)> with get, set
    //abstract OnInputTouchLeave: UnrealEngineMulticastDelegate<(ETouchIndex -> PrimitiveComponent -> unit)> with get, set
    abstract LODParentPrimitive: PrimitiveComponent with get, set
    //abstract PostPhysicsComponentTick: PrimitiveComponentPostPhysicsTickFunction with get, set
    abstract WakeRigidBody: BoneName: string -> unit
    abstract WakeAllRigidBodies: unit -> unit
    //abstract SetWalkableSlopeOverride: NewOverride: WalkableSlopeOverride -> unit
    abstract SetUseCCD: InUseCCD: bool * BoneName: string -> unit
    abstract SetTranslucentSortPriority: NewTranslucentSortPriority: float -> unit
    abstract SetSingleSampleShadowFromStationaryLights: bNewSingleSampleShadowFromStationaryLights: bool -> unit
    abstract SetSimulatePhysics: bSimulate: bool -> unit
    abstract SetRenderInMono: bValue: bool -> unit
    abstract SetRenderInMainPass: bValue: bool -> unit
    abstract SetRenderCustomDepth: bValue: bool -> unit
    abstract SetReceivesDecals: bNewReceivesDecals: bool -> unit
    //abstract SetPhysMaterialOverride: NewPhysMaterial: PhysicalMaterial -> unit
    abstract SetPhysicsMaxAngularVelocityInRadians: NewMaxAngVel: float * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsMaxAngularVelocityInDegrees: NewMaxAngVel: float * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsMaxAngularVelocity: NewMaxAngVel: float * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsLinearVelocity: NewVel: Vector * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsAngularVelocityInRadians: NewAngVel: Vector * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsAngularVelocityInDegrees: NewAngVel: Vector * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetPhysicsAngularVelocity: NewAngVel: Vector * bAddToCurrent: bool * BoneName: string -> unit
    abstract SetOwnerNoSee: bNewOwnerNoSee: bool -> unit
    abstract SetOnlyOwnerSee: bNewOnlyOwnerSee: bool -> unit
    abstract SetNotifyRigidBodyCollision: bNewNotifyRigidBodyCollision: bool -> unit
    abstract SetMaterialByName: MaterialSlotName: string * Material: MaterialInterface -> unit
    abstract SetMaterial: ElementIndex: float * Material: MaterialInterface -> unit
    abstract SetMassScale: BoneName: string * InMassScale: float -> unit
    abstract SetMassOverrideInKg: BoneName: string * MassInKg: float * bOverrideMass: bool -> unit
    //abstract SetLockedAxis: LockedAxis: EDOFMode -> unit
    abstract SetLinearDamping: InDamping: float -> unit
    abstract SetGenerateOverlapEvents: bInGenerateOverlapEvents: bool -> unit
    abstract SetEnableGravity: bGravityEnabled: bool -> unit
    //abstract SetCustomDepthStencilWriteMask: WriteMaskBit: ERendererStencilMask -> unit
    abstract SetCustomDepthStencilValue: Value: float -> unit
    abstract SetCullDistance: NewCullDistance: float -> unit
    //abstract SetConstraintMode: ConstraintMode: EDOFMode -> unit
    //abstract SetCollisionResponseToChannel: Channel: ECollisionChannel * NewResponse: ECollisionResponse -> unit
    //abstract SetCollisionResponseToAllChannels: NewResponse: ECollisionResponse -> unit
    abstract SetCollisionProfileName: InCollisionProfileName: string -> unit
    //abstract SetCollisionObjectType: Channel: ECollisionChannel -> unit
    //abstract SetCollisionEnabled: NewType: ECollisionEnabled -> unit
    abstract SetCenterOfMass: CenterOfMassOffset: Vector * BoneName: string -> unit
    abstract SetCastShadow: NewCastShadow: bool -> unit
    abstract SetBoundsScale: NewBoundsScale: float -> unit
    abstract SetAngularDamping: InDamping: float -> unit
    abstract SetAllUseCCD: InUseCCD: bool -> unit
    abstract SetAllPhysicsLinearVelocity: NewVel: Vector * bAddToCurrent: bool -> unit
    abstract SetAllPhysicsAngularVelocityInRadians: NewAngVel: Vector * bAddToCurrent: bool -> unit
    abstract SetAllPhysicsAngularVelocityInDegrees: NewAngVel: Vector * bAddToCurrent: bool -> unit
    abstract SetAllPhysicsAngularVelocity: NewAngVel: Vector * bAddToCurrent: bool -> unit
    abstract SetAllMassScale: InMassScale: float -> unit
    abstract ScaleByMomentOfInertia: InputVector: Vector * BoneName: string -> Vector
    abstract PutRigidBodyToSleep: BoneName: string -> unit
    //abstract K2_SphereTraceComponent: TraceStart: Vector * TraceEnd: Vector * SphereRadius: float * bTraceComplex: bool * bShowTrace: bool * bPersistentShowTrace: bool * ?HitLocation: Vector * ?HitNormal: Vector * ?BoneName: string * ?OutHit: HitResult -> obj
    //abstract K2_SphereOverlapComponent: InSphereCentre: Vector * InSphereRadius: float * bTraceComplex: bool * bShowTrace: bool * bPersistentShowTrace: bool * ?HitLocation: Vector * ?HitNormal: Vector * ?BoneName: string * ?OutHit: HitResult -> obj
    //abstract K2_LineTraceComponent: TraceStart: Vector * TraceEnd: Vector * bTraceComplex: bool * bShowTrace: bool * bPersistentShowTrace: bool * ?HitLocation: Vector * ?HitNormal: Vector * ?BoneName: string * ?OutHit: HitResult -> obj
    abstract K2_IsQueryCollisionEnabled: unit -> bool
    abstract K2_IsPhysicsCollisionEnabled: unit -> bool
    abstract K2_IsCollisionEnabled: unit -> bool
    //abstract K2_BoxOverlapComponent: InBoxCentre: Vector * InBox: Box * bTraceComplex: bool * bShowTrace: bool * bPersistentShowTrace: bool * ?HitLocation: Vector * ?HitNormal: Vector * ?BoneName: string * ?OutHit: HitResult -> obj
    abstract IsOverlappingComponent: OtherComp: PrimitiveComponent -> bool
    abstract IsOverlappingActor: Other: Actor -> bool
    abstract IsGravityEnabled: unit -> bool
    abstract IsAnyRigidBodyAwake: unit -> bool
    abstract IgnoreComponentWhenMoving: Component: PrimitiveComponent * bShouldIgnore: bool -> unit
    abstract IgnoreActorWhenMoving: Actor: Actor * bShouldIgnore: bool -> unit
    //abstract GetWalkableSlopeOverride: unit -> WalkableSlopeOverride
    abstract GetPhysicsLinearVelocityAtPoint: Point: Vector * BoneName: string -> Vector
    abstract GetPhysicsLinearVelocity: BoneName: string -> Vector
    abstract GetPhysicsAngularVelocityInRadians: BoneName: string -> Vector
    abstract GetPhysicsAngularVelocityInDegrees: BoneName: string -> Vector
    abstract GetPhysicsAngularVelocity: BoneName: string -> Vector
    abstract GetOverlappingComponents: ?OutOverlappingComponents: ResizeArray<PrimitiveComponent> -> obj
    abstract GetOverlappingActors: ?OverlappingActors: ResizeArray<Actor> * ?ClassFilter: UnrealEngineClass -> obj
    abstract GetNumMaterials: unit -> float
    abstract GetMaterialFromCollisionFaceIndex: FaceIndex: float * ?SectionIndex: float -> obj
    abstract GetMaterial: ElementIndex: float -> MaterialInterface
    abstract GetMassScale: BoneName: string -> float
    abstract GetMass: unit -> float
    abstract GetLinearDamping: unit -> float
    abstract GetInertiaTensor: BoneName: string -> Vector
    abstract GetGenerateOverlapEvents: unit -> bool
    //abstract GetCollisionResponseToChannel: Channel: ECollisionChannel -> ECollisionResponse
    abstract GetCollisionProfileName: unit -> string
    //abstract GetCollisionObjectType: unit -> ECollisionChannel
    //abstract GetCollisionEnabled: unit -> ECollisionEnabled
    abstract GetClosestPointOnCollision: Point: Vector * ?OutPointOnBody: Vector * ?BoneName: string -> obj
    abstract GetCenterOfMass: BoneName: string -> Vector
    abstract GetAngularDamping: unit -> float
    //abstract CreateDynamicMaterialInstance: ElementIndex: float * SourceMaterial: MaterialInterface * OptionalName: string -> MaterialInstanceDynamic
    //abstract CreateAndSetMaterialInstanceDynamicFromMaterial: ElementIndex: float * Parent: MaterialInterface -> MaterialInstanceDynamic
    //abstract CreateAndSetMaterialInstanceDynamic: ElementIndex: float -> MaterialInstanceDynamic
    abstract CopyArrayOfMoveIgnoreComponents: unit -> ResizeArray<PrimitiveComponent>
    abstract CopyArrayOfMoveIgnoreActors: unit -> ResizeArray<Actor>
    abstract ClearMoveIgnoreComponents: unit -> unit
    abstract ClearMoveIgnoreActors: unit -> unit
    //abstract CanCharacterStepUp: Pawn: Pawn -> bool
    abstract AddTorqueInRadians: Torque: Vector * BoneName: string * bAccelChange: bool -> unit
    abstract AddTorqueInDegrees: Torque: Vector * BoneName: string * bAccelChange: bool -> unit
    abstract AddTorque: Torque: Vector * BoneName: string * bAccelChange: bool -> unit
    abstract AddRadialImpulse: Origin: Vector * Radius: float * Strength: float * Falloff: ERadialImpulseFalloff * bVelChange: bool -> unit
    abstract AddRadialForce: Origin: Vector * Radius: float * Strength: float * Falloff: ERadialImpulseFalloff * bAccelChange: bool -> unit
    abstract AddImpulseAtLocation: Impulse: Vector * Location: Vector * BoneName: string -> unit
    abstract AddImpulse: Impulse: Vector * BoneName: string * bVelChange: bool -> unit
    abstract AddForceAtLocationLocal: Force: Vector * Location: Vector * BoneName: string -> unit
    abstract AddForceAtLocation: Force: Vector * Location: Vector * BoneName: string -> unit
    abstract AddForce: Force: Vector * BoneName: string * bAccelChange: bool -> unit
    abstract AddAngularImpulseInRadians: Impulse: Vector * BoneName: string * bVelChange: bool -> unit
    abstract AddAngularImpulseInDegrees: Impulse: Vector * BoneName: string * bVelChange: bool -> unit
    abstract AddAngularImpulse: Impulse: Vector * BoneName: string * bVelChange: bool -> unit
    //abstract ComponentOverlapActors: ComponentTransform: Transform * ObjectTypes: ResizeArray<EObjectTypeQuery> * ActorClassFilter: UnrealEngineClass * ActorsToIgnore: ResizeArray<Actor> * ?OutActors: ResizeArray<Actor> -> obj
    //abstract ComponentOverlapComponents: ComponentTransform: Transform * ObjectTypes: ResizeArray<EObjectTypeQuery> * ComponentClassFilter: UnrealEngineClass * ActorsToIgnore: ResizeArray<Actor> * ?OutComponents: ResizeArray<PrimitiveComponent> -> obj

type [<AllowNullLiteral>] PrimitiveComponentStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> PrimitiveComponent
    abstract Load: ResourceName: string -> PrimitiveComponent
    abstract Find: Outer: UObject * ResourceName: string -> PrimitiveComponent
    abstract GetDefaultObject: unit -> PrimitiveComponent
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> PrimitiveComponent
    abstract C: Other: U2<UObject, obj option> -> PrimitiveComponent
    //abstract ComponentOverlapActors: Component: PrimitiveComponent * ComponentTransform: Transform * ObjectTypes: ResizeArray<EObjectTypeQuery> * ActorClassFilter: UnrealEngineClass * ActorsToIgnore: ResizeArray<Actor> * ?OutActors: ResizeArray<Actor> -> obj
    //abstract ComponentOverlapComponents: Component: PrimitiveComponent * ComponentTransform: Transform * ObjectTypes: ResizeArray<EObjectTypeQuery> * ComponentClassFilter: UnrealEngineClass * ActorsToIgnore: ResizeArray<Actor> * ?OutComponents: ResizeArray<PrimitiveComponent> -> obj

type [<StringEnum>] [<RequireQualifiedAccess>] EHorizTextAligment =
    | [<CompiledName "EHTA_Left">] EHTA_Left
    | [<CompiledName "EHTA_Center">] EHTA_Center
    | [<CompiledName "EHTA_Right">] EHTA_Right
    | [<CompiledName "EHTA_MAX">] EHTA_MAX

type [<StringEnum>] [<RequireQualifiedAccess>] EVerticalTextAligment =
    | [<CompiledName "EVRTA_TextTop">] EVRTA_TextTop
    | [<CompiledName "EVRTA_TextCenter">] EVRTA_TextCenter
    | [<CompiledName "EVRTA_TextBottom">] EVRTA_TextBottom
    | [<CompiledName "EVRTA_QuadTop">] EVRTA_QuadTop
    | [<CompiledName "EVRTA_MAX">] EVRTA_MAX

type [<AllowNullLiteral>] TextRenderComponent =
    inherit PrimitiveComponent
    abstract text: string with get, set
    abstract TextMaterial: MaterialInterface with get, set
    abstract Font: Font with get, set
    abstract HorizontalAlignment: EHorizTextAligment with get, set
    abstract VerticalAlignment: EVerticalTextAligment with get, set
    abstract TextRenderColor: Color with get, set
    abstract XScale: float with get, set
    abstract YScale: float with get, set
    abstract WorldSize: float with get, set
    abstract InvDefaultSize: float with get, set
    abstract HorizSpacingAdjust: float with get, set
    abstract VertSpacingAdjust: float with get, set
    abstract bAlwaysRenderAsText: bool with get, set
    abstract SetYScale: Value: float -> unit
    abstract SetXScale: Value: float -> unit
    abstract SetWorldSize: Value: float -> unit
    abstract SetVertSpacingAdjust: Value: float -> unit
    abstract SetVerticalAlignment: Value: EVerticalTextAligment -> unit
    abstract SetTextRenderColor: Value: Color -> unit
    abstract SetTextMaterial: Material: MaterialInterface -> unit
    abstract SetText: Value: string -> unit
    abstract SetHorizSpacingAdjust: Value: float -> unit
    abstract SetHorizontalAlignment: Value: EHorizTextAligment -> unit
    abstract SetFont: Value: Font -> unit
    abstract K2_SetText: Value: string -> unit
    abstract GetTextWorldSize: unit -> Vector
    abstract GetTextLocalSize: unit -> Vector

type [<AllowNullLiteral>] TextureParameterValue =
    abstract ParameterName: string with get, set
    //abstract ParameterInfo: MaterialParameterInfo with get, set
    //abstract ParameterValue: Texture with get, set
    abstract ExpressionGUID: Guid with get, set
    abstract clone: unit -> TextureParameterValue

type [<AllowNullLiteral>] TextureParameterValueStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> TextureParameterValue
    abstract C: Other: U2<UObject, obj option> -> TextureParameterValue

type [<StringEnum>] [<RequireQualifiedAccess>] EFontCacheType =
    | [<CompiledName "Offline">] Offline
    | [<CompiledName "Runtime">] Runtime
    | [<CompiledName "EFontCacheType_MAX">] EFontCacheType_MAX

type [<AllowNullLiteral>] FontCharacter =
    abstract StartU: float with get, set
    abstract StartV: float with get, set
    abstract USize: float with get, set
    abstract VSize: float with get, set
    abstract TextureIndex: float with get, set
    abstract VerticalOffset: float with get, set
    abstract clone: unit -> FontCharacter

type [<AllowNullLiteral>] FontCharacterStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> FontCharacter
    abstract C: Other: U2<UObject, obj option> -> FontCharacter

type [<StringEnum>] [<RequireQualifiedAccess>] EFontImportCharacterSet =
    | [<CompiledName "FontICS_Default">] FontICS_Default
    | [<CompiledName "FontICS_Ansi">] FontICS_Ansi
    | [<CompiledName "FontICS_Symbol">] FontICS_Symbol
    | [<CompiledName "FontICS_MAX">] FontICS_MAX

type [<AllowNullLiteral>] FontImportOptionsData =
    abstract FontName: string with get, set
    abstract Height: float with get, set
    abstract bEnableAntialiasing: bool with get, set
    abstract bEnableBold: bool with get, set
    abstract bEnableItalic: bool with get, set
    abstract bEnableUnderline: bool with get, set
    abstract bAlphaOnly: bool with get, set
    abstract CharacterSet: EFontImportCharacterSet with get, set
    abstract Chars: string with get, set
    abstract UnicodeRange: string with get, set
    abstract CharsFilePath: string with get, set
    abstract CharsFileWildcard: string with get, set
    abstract bCreatePrintableOnly: bool with get, set
    abstract bIncludeASCIIRange: bool with get, set
    abstract ForegroundColor: LinearColor with get, set
    abstract bEnableDropShadow: bool with get, set
    abstract TexturePageWidth: float with get, set
    abstract TexturePageMaxHeight: float with get, set
    abstract XPadding: float with get, set
    abstract YPadding: float with get, set
    abstract ExtendBoxTop: float with get, set
    abstract ExtendBoxBottom: float with get, set
    abstract ExtendBoxRight: float with get, set
    abstract ExtendBoxLeft: float with get, set
    abstract bEnableLegacyMode: bool with get, set
    abstract Kerning: float with get, set
    abstract bUseDistanceFieldAlpha: bool with get, set
    abstract DistanceFieldScaleFactor: float with get, set
    abstract DistanceFieldScanRadiusScale: float with get, set
    abstract clone: unit -> FontImportOptionsData

type [<AllowNullLiteral>] FontImportOptionsDataStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> FontImportOptionsData
    abstract C: Other: U2<UObject, obj option> -> FontImportOptionsData

type [<StringEnum>] [<RequireQualifiedAccess>] EFontHinting =
    | [<CompiledName "Default">] Default
    | [<CompiledName "Auto">] Auto
    | [<CompiledName "AutoLight">] AutoLight
    | [<CompiledName "Monochrome">] Monochrome
    | [<CompiledName "None">] None
    | [<CompiledName "EFontHinting_MAX">] EFontHinting_MAX

type [<StringEnum>] [<RequireQualifiedAccess>] EFontLoadingPolicy =
    | [<CompiledName "LazyLoad">] LazyLoad
    | [<CompiledName "Stream">] Stream
    | [<CompiledName "Inline">] Inline
    | [<CompiledName "EFontLoadingPolicy_MAX">] EFontLoadingPolicy_MAX

type [<AllowNullLiteral>] FontBulkData =
    inherit UObject

type [<AllowNullLiteral>] FontBulkDataStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> FontBulkData
    abstract Load: ResourceName: string -> FontBulkData
    abstract Find: Outer: UObject * ResourceName: string -> FontBulkData
    abstract GetDefaultObject: unit -> FontBulkData
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> FontBulkData
    abstract C: Other: U2<UObject, obj option> -> FontBulkData

type [<AllowNullLiteral>] FontData =
    abstract FontFilename: string with get, set
    abstract Hinting: EFontHinting with get, set
    abstract LoadingPolicy: EFontLoadingPolicy with get, set
    abstract SubFaceIndex: float with get, set
    abstract FontFaceAsset: UObject with get, set
    abstract BulkDataPtr: FontBulkData with get, set
    abstract FontData: ResizeArray<float> with get, set
    abstract clone: unit -> FontData

type [<AllowNullLiteral>] FontDataStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> FontData
    abstract C: Other: U2<UObject, obj option> -> FontData

type [<AllowNullLiteral>] TypefaceEntry =
    abstract Name: string with get, set
    abstract Font: FontData with get, set
    abstract clone: unit -> TypefaceEntry

type [<AllowNullLiteral>] TypefaceEntryStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> TypefaceEntry
    abstract C: Other: U2<UObject, obj option> -> TypefaceEntry

type [<AllowNullLiteral>] Typeface =
    abstract Fonts: ResizeArray<TypefaceEntry> with get, set
    abstract clone: unit -> Typeface

type [<AllowNullLiteral>] TypefaceStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Typeface
    abstract C: Other: U2<UObject, obj option> -> Typeface

type [<AllowNullLiteral>] CompositeFallbackFont =
    abstract Typeface: Typeface with get, set
    abstract ScalingFactor: float with get, set
    abstract clone: unit -> CompositeFallbackFont

type [<AllowNullLiteral>] CompositeFallbackFontStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> CompositeFallbackFont
    abstract C: Other: U2<UObject, obj option> -> CompositeFallbackFont

type [<StringEnum>] [<RequireQualifiedAccess>] ERangeBoundTypes =
    | [<CompiledName "Exclusive">] Exclusive
    | [<CompiledName "Inclusive">] Inclusive
    | [<CompiledName "Open">] Open
    | [<CompiledName "ERangeBoundTypes_MAX">] ERangeBoundTypes_MAX

type [<AllowNullLiteral>] Int32RangeBound =
    abstract Type: ERangeBoundTypes with get, set
    abstract Value: float with get, set
    abstract clone: unit -> Int32RangeBound

type [<AllowNullLiteral>] Int32RangeBoundStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Int32RangeBound
    abstract C: Other: U2<UObject, obj option> -> Int32RangeBound

type [<AllowNullLiteral>] Int32Range =
    abstract LowerBound: Int32RangeBound with get, set
    abstract UpperBound: Int32RangeBound with get, set
    abstract clone: unit -> Int32Range

type [<AllowNullLiteral>] Int32RangeStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Int32Range
    abstract C: Other: U2<UObject, obj option> -> Int32Range

type [<AllowNullLiteral>] CompositeSubFont =
    inherit CompositeFallbackFont
    abstract CharacterRanges: ResizeArray<Int32Range> with get, set
    abstract Cultures: string with get, set
    abstract EditorName: string with get, set
    abstract clone: unit -> CompositeSubFont

type [<AllowNullLiteral>] CompositeSubFontStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> CompositeSubFont
    abstract C: Other: U2<UObject, obj option> -> CompositeSubFont

type [<AllowNullLiteral>] CompositeFont =
    abstract DefaultTypeface: Typeface with get, set
    abstract FallbackTypeface: CompositeFallbackFont with get, set
    abstract SubTypefaces: ResizeArray<CompositeSubFont> with get, set
    abstract clone: unit -> CompositeFont

type [<AllowNullLiteral>] CompositeFontStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> CompositeFont
    abstract C: Other: U2<UObject, obj option> -> CompositeFont

type [<AllowNullLiteral>] Font =
    inherit UObject
    abstract FontCacheType: EFontCacheType with get, set
    abstract Characters: ResizeArray<FontCharacter> with get, set
    //abstract Textures: ResizeArray<Texture2D> with get, set
    abstract IsRemapped: float with get, set
    abstract EmScale: float with get, set
    abstract Ascent: float with get, set
    abstract Descent: float with get, set
    abstract Leading: float with get, set
    abstract Kerning: float with get, set
    abstract ImportOptions: FontImportOptionsData with get, set
    abstract NumCharacters: float with get, set
    abstract MaxCharHeight: ResizeArray<float> with get, set
    abstract ScalingFactor: float with get, set
    abstract LegacyFontSize: float with get, set
    abstract LegacyFontName: string with get, set
    abstract CompositeFont: CompositeFont with get, set

type [<AllowNullLiteral>] FontStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> Font
    abstract Load: ResourceName: string -> Font
    abstract Find: Outer: UObject * ResourceName: string -> Font
    abstract GetDefaultObject: unit -> Font
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> Font
    abstract C: Other: U2<UObject, obj option> -> Font

type [<AllowNullLiteral>] FontParameterValue =
    abstract ParameterName: string with get, set
    //abstract ParameterInfo: MaterialParameterInfo with get, set
    abstract FontValue: Font with get, set
    abstract FontPage: float with get, set
    abstract ExpressionGUID: Guid with get, set
    abstract clone: unit -> FontParameterValue

type [<AllowNullLiteral>] FontParameterValueStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> FontParameterValue
    abstract C: Other: U2<UObject, obj option> -> FontParameterValue

type [<AllowNullLiteral>] TextRenderComponentStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> TextRenderComponent
    abstract Load: ResourceName: string -> TextRenderComponent
    abstract Find: Outer: UObject * ResourceName: string -> TextRenderComponent
    abstract GetDefaultObject: unit -> TextRenderComponent
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> TextRenderComponent
    abstract C: Other: U2<UObject, obj option> -> TextRenderComponent

type [<AllowNullLiteral>] TextRenderActor =
    inherit Actor
    abstract TextRender: TextRenderComponent with get, set
    //abstract SpriteComponent: BillboardComponent with get, set

type [<AllowNullLiteral>] TextRenderActorStatic =
    [<Emit "new $0($1...)">] abstract Create: unit -> TextRenderActor
    [<Emit "new $0($1...)">] abstract Create: InWorld: World * ?Location: Vector * ?Rotation: Rotator -> TextRenderActor
    abstract GetDefaultObject: unit -> TextRenderActor
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> TextRenderActor
    abstract C: Other: U2<UObject, obj option> -> TextRenderActor

type [<AllowNullLiteral>] ActorStatic =
    [<Emit "new $0($1...)">] abstract Create: InWorld: World * ?Location: Vector * ?Rotation: Rotator -> Actor
    abstract GetDefaultObject: unit -> Actor
    abstract CreateDefaultSubobject: Name: string * ?Transient: bool * ?Required: bool * ?Abstract: bool -> Actor
    abstract C: Other: U2<UObject, obj option> -> Actor
    abstract ClearActorLabel: Actor: Actor -> unit
    abstract GetActorLabel: Actor: Actor -> string
    abstract GetFolderPath: Actor: Actor -> string
    abstract IsActorLabelEditable: Actor: Actor -> bool
    abstract SetActorLabel: Actor: Actor * NewActorLabel: string * bMarkDirty: bool -> unit
    abstract SetFolderPath: Actor: Actor * NewFolderPath: string -> unit
    abstract SetFolderPath_Recursively: Actor: Actor * NewFolderPath: string -> unit
    abstract SetIsTemporarilyHiddenInEditor: Actor: Actor * bIsHidden: bool -> unit
    abstract Actor_GetWorld: Actor: Actor -> World
    abstract GetLastRenderTime: Actor: Actor -> float
    //abstract GetLevel: Actor: Actor -> Level
    abstract IsPendingKill: InActor: Actor -> bool
    abstract ReregisterAllComponents: Actor: Actor -> unit
    abstract SetActorFlags: Actor: Actor * Flags: float -> unit
    abstract SetRootComponent: Actor: Actor * Component: SceneComponent -> unit
    abstract SetFocusActor: InFocusActor: Actor -> unit
    abstract GetActorBounds: Actor: Actor * ?Origin: Vector * ?BoxExtent: Vector -> obj
    //abstract ApplyDamage: DamagedActor: Actor * BaseDamage: float * EventInstigator: Controller * DamageCauser: Actor * DamageTypeClass: UnrealEngineClass -> float
    //abstract ApplyPointDamage: DamagedActor: Actor * BaseDamage: float * HitFromDirection: Vector * HitInfo: HitResult * EventInstigator: Controller * DamageCauser: Actor * DamageTypeClass: UnrealEngineClass -> float
    abstract FinishSpawningActor: Actor: Actor * SpawnTransform: Transform -> Actor
    //abstract GetAIController: ControlledActor: Actor -> AIController
    //abstract GetBlackboard: Target: Actor -> BlackboardComponent
    //abstract AddDeviceVisualizationComponentBlocking: Target: Actor * XRDeviceId: XRDeviceId * bManualAttachment: bool * RelativeTransform: Transform -> PrimitiveComponent
    //abstract AddNamedDeviceVisualizationComponentBlocking: Target: Actor * SystemName: string * DeviceName: string * bManualAttachment: bool * RelativeTransform: Transform * ?XRDeviceId: XRDeviceId -> obj

type World =
    inherit UObject

let [<Global>] GWorld: World = jsNative

let [<Global>] Vector : VectorStatic = jsNative   
let [<Global>] Rotator : RotatorStatic  = jsNative
let [<Global>] Actor : ActorStatic  = jsNative
let [<Global>] TextRenderActor : TextRenderActorStatic  = jsNative
